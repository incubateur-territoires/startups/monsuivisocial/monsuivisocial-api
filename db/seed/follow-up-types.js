module.exports = {
  customFollowUpTypesData: [
    {
      follow_up_types_id: {
        name: "Secours d'urgence",
        type: 'optional',
        default: false
      }
    },
    {
      follow_up_types_id: {
        name: 'PASS Adulte',
        type: 'optional',
        default: false
      }
    },
    {
      follow_up_types_id: {
        name: 'Aide à la culture',
        type: 'optional',
        default: false
      }
    },
    {
      follow_up_types_id: {
        name: 'Aide au relogement',
        type: 'optional',
        default: false
      }
    }
  ],

  defaultFollowUpTypeNamesData: [
    'Domiciliation',
    "Aide médicale d'État",
    'Aide sociale',
    'Revenu de solidarité active',
    'Aides financières non remboursables',
    'Aides financières remboursables',
    'Inclusion numérique',
    'Aide alimentaire',
    'Autre'
  ]
}
