const { Directus } = require('@directus/sdk')
const { beneficiariesData } = require('./beneficiaries')
const {
  customFollowUpTypesData,
  defaultFollowUpTypeNamesData
} = require('./follow-up-types')
const { organisationData } = require('./organisation')
const { usersData } = require('./users')
require('dotenv').config()

const directusUrl = process.env.PUBLIC_URL.replace('localhost', '127.0.0.1') // Somehow, axios is not able to request localhost

const directus = new Directus(directusUrl)

let organisation
// let followUpTypes // TODO
let users
// let beneficiaries // TODO

async function start() {
  // But, we need to authenticate if data is private
  let authenticated = false

  // Try to authenticate with token if exists
  try {
    await directus.auth.refresh()
    authenticated = true
  } catch (err) {}

  // Let's login in case we don't have token or it is invalid / expired
  if (!authenticated) {
    try {
      await directus.auth.login({
        email: process.env.ADMIN_EMAIL,
        password: process.env.ADMIN_PASSWORD
      })
      authenticated = true
    } catch (err) {
      console.error(err)
      throw new Error(
        'Invalid credentials - Make sure you have set ADMIN_EMAIL and ADMIN_PASSWORD values in the .env config.'
      )
    }
  }
}

async function getDefaultFollowUpTypes() {
  const { data } = await directus.items('follow_up_types').readByQuery({
    fields: 'id',
    filter: {
      default: { _eq: true },
      name: { _in: defaultFollowUpTypeNamesData }
    }
  })
  return data
}

async function seedOrganisation(organisationFollowUpTypes) {
  const organisations = directus.items('organisation')

  try {
    return await organisations.createOne(
      organisationData(organisationFollowUpTypes),
      { fields: '*,follow_up_types.*' }
    )
  } catch (err) {
    console.error('The organisation creation has failed.')
    console.error(err)
  }
}

async function seedUsers() {
  try {
    return (await directus.users.createMany(usersData(organisation.id))).data
  } catch (err) {
    console.error('The users creation has failed.')
    console.error(err)
  }
}

async function seedBeneficiaries() {
  const beneficiaries = directus.items('beneficiary')

  try {
    return (await beneficiaries.createMany(beneficiariesData(users))).data
  } catch (err) {
    console.error('The beneficiaries creation has failed.')
    console.error(err)
  }
}

start()
  .then(() => getDefaultFollowUpTypes())
  .then(defaultFollowUpTypes => {
    const organisationFollowUpTypes = defaultFollowUpTypes.map(({ id }) => ({
      follow_up_types_id: id
    }))
    organisationFollowUpTypes.push(...customFollowUpTypesData)

    return seedOrganisation(organisationFollowUpTypes)
  })
  .then(_organisation => {
    console.log(`The organisation ${_organisation.name} has been created.`)
    organisation = _organisation
    // followUpTypes = _organisation.follow_up_types.map(f => f.follow_up_types_id) // TODO
    return seedUsers()
  })
  .then(_users => {
    _users.forEach(user => {
      console.log(
        `The user ${user.first_name} ${user.last_name} has been created.`
      )
    })
    users = _users
    return seedBeneficiaries()
  })
  .then(_beneficiaries => {
    _beneficiaries.forEach(beneficiary => {
      console.log(
        `The beneficiary ${beneficiary.firstname} ${beneficiary.usual_name} has been created.`
      )
    })
    // beneficiaries = _beneficiaries
  })
