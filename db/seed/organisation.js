module.exports = {
  organisationData: followUpTypes => ({
    type: 'ccas',
    name: 'CCAS de Test',
    zip_code: '00000',
    city: 'Ennui-sur-Blasé',
    address: '1 rue du Ruisseau',
    phone: '00 00 00 00 00',
    email: 'ccas-test@example.com',
    follow_up_types: followUpTypes
  })
}
