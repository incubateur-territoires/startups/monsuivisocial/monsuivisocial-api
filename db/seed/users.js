const {
  RESPONSABLE_STRUCTURE_ID,
  TRAVAILLEUR_SOCIAL_ID,
  REFERENT_ID,
  INSTRUCTEUR_ID,
  AGENT_D_ACCUEIL_ID
} = require('../../utils/constants')

module.exports = {
  usersData: organisationId => [
    {
      first_name: 'Jeanne',
      last_name: 'Responsable',
      email: 'jeanne.responsable@example.com',
      role: RESPONSABLE_STRUCTURE_ID,
      organisation: organisationId,
      provider: 'keycloak',
      external_identifier: 'jeanne.responsable@example.com'
    },
    {
      first_name: 'Jean',
      last_name: 'Travailleur social',
      email: 'jean.travailleur-social@example.com',
      role: TRAVAILLEUR_SOCIAL_ID,
      organisation: organisationId,
      provider: 'keycloak',
      external_identifier: 'jean.travailleur-social@example.com'
    },
    {
      first_name: 'Jeanne',
      last_name: 'Référente',
      email: 'jeanne.référente@example.com',
      role: REFERENT_ID,
      organisation: organisationId,
      provider: 'keycloak',
      external_identifier: 'jeanne.référente@example.com'
    },
    {
      first_name: 'Jeanne',
      last_name: 'Instructeur',
      email: 'jeanne.instructeure@example.com',
      role: INSTRUCTEUR_ID,
      organisation: organisationId,
      provider: 'keycloak',
      external_identifier: 'jeanne.instructeure@example.com'
    },
    {
      first_name: 'Jean',
      last_name: 'Agent',
      email: 'jean.agent@example.com',
      role: AGENT_D_ACCUEIL_ID,
      organisation: organisationId,
      provider: 'keycloak',
      external_identifier: 'jean.agent@example.com'
    }
  ]
}
