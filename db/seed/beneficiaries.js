module.exports = {
  beneficiariesData: users => [
    {
      user_created: users[0].id,
      referents: [{ referent: users[3].id }, { referent: users[0].id }],
      status: 'active',
      title: 'lady',
      usual_name: 'Wonfar',
      birth_name: 'Ridewood',
      firstname: 'Lóng',
      gender: 'lady',
      email: 'lridewood0@de.vu'
    },
    {
      user_created: users[0].id,
      referents: [{ referent: users[0].id }],
      status: 'active',
      title: 'man',
      usual_name: 'Apfelmann',
      firstname: 'Aurélie',
      gender: 'lady'
    },
    {
      user_created: users[0].id,
      referents: [
        { referent: users[1].id },
        { referent: users[4].id },
        { referent: users[3].id }
      ],
      status: 'active',
      title: 'man',
      usual_name: 'Piscopo',
      firstname: 'Valérie',
      gender: 'man'
    },
    {
      user_created: users[0].id,
      referents: [{ referent: users[2].id }, { referent: users[4].id }],
      status: 'deceased',
      title: 'lady',
      usual_name: 'Breitling',
      firstname: 'Léa',
      deathdate: '2014-06-29',
      email: 'mpitone3@rambler.ru'
    },
    {
      user_created: users[0].id,
      referents: [{ referent: users[3].id }],
      status: 'active',
      usual_name: 'Downer',
      firstname: 'Anaé',
      gender: 'other'
    },
    {
      referents: [{ referent: users[4].id }, { referent: users[1].id }],
      status: 'inactive',
      usual_name: 'Heinig',
      birth_name: 'Hunton',
      firstname: 'Mélia',
      gender: 'lady'
    },
    {
      user_created: users[0].id,
      referents: [{ referent: users[0].id }],
      status: 'active',
      usual_name: 'Penberthy',
      firstname: 'Léonore',
      birthdate: '1965-10-08',
      gender: 'man',
      email: 'rsword6@wp.com'
    },
    {
      user_created: users[0].id,
      referents: [{ referent: users[0].id }, { referent: users[2].id }],
      status: 'deceased',
      title: 'man',
      usual_name: 'Penson',
      firstname: 'Táng',
      deathdate: '2017-08-29',
      gender: 'lady',
      email: 'jwedderburn7@businesswire.com'
    },
    {
      user_created: users[0].id,
      referents: [{ referent: users[0].id }],
      status: 'active',
      usual_name: 'Coursor',
      birth_name: 'Blankett',
      firstname: 'Gisèle',
      birthdate: '1987-08-21',
      email: 'cblankett8@histats.com'
    },
    {
      user_created: users[0].id,
      referents: [{ referent: users[1].id }],
      status: 'archived',
      usual_name: 'Tearle',
      birth_name: 'Firth',
      firstname: 'Måns'
    }
  ]
}
