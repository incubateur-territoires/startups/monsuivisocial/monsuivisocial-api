const { PERMISSIONS_ID_SEQ } = require('./constants')

/**
 * Set a new sequence value
 * @param knex The knex instance
 * @param name The sequence name
 * @param value The sequence number to set
 * @param isUsed Whether the sequence number is already used or not. Default: true
 */
async function setSequenceValue(knex, name, value, isUsed = true) {
  await knex.raw(`
      SELECT pg_catalog.setval('${name}', ${value}, ${isUsed});
    `)
}

/**
 * Get the current sequence value
 * @param knex The knex instance
 * @param name The sequence name
 */
async function getSequenceValue(knex, name) {
  const { rows } = await knex.raw(`
      SELECT last_value FROM ${name};
    `)

  return rows[0].last_value
}

/**
 *
 * @param knex The knex instance
 * @param role The id of the role associated to the permission
 * @param collection The collection of the permission
 * @param action The action of the permissions (create, read, update, delete)
 * @param permissions The permission representation as an object
 * @param fields Optional - The fields related to the permission represented as a string list separated by comas
 */
async function insertPermission(
  knex,
  role,
  collection,
  action,
  permissions,
  fields
) {
  const sequenceId = parseInt(await getSequenceValue(knex, PERMISSIONS_ID_SEQ))

  const permission = { role, collection, action, permissions }
  if (fields) permission.fields = fields

  await knex('directus_permissions').insert(permission)

  await setSequenceValue(knex, PERMISSIONS_ID_SEQ, sequenceId + 1)
}

module.exports = {
  setSequenceValue,
  getSequenceValue,
  insertPermission
}
