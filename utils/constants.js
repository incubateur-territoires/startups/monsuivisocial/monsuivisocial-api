// Roles
const RESPONSABLE_STRUCTURE_ID = '77c48d8b-a130-4a49-a00a-53a564bb9dbe'
const RESPONSABLE_STRUCTURE_SLUG = 'responsable-de-structure'

const TRAVAILLEUR_SOCIAL_ID = 'c622eb01-b206-4f50-8c8b-66b6b7c1bcee'
const TRAVAILLEUR_SOCIAL_SLUG = 'travailleur-social'

const REFERENT_ID = '810fa339-9f91-48f5-9278-dea2c0f0ff18'
const REFERENT_SLUG = 'referent'

const INSTRUCTEUR_ID = 'bd87425f-5ebc-416b-906f-b367914dc4c2'
const INSTRUCTEUR_SLUG = 'instructeur'

const AGENT_D_ACCUEIL_ID = '4eee829e-d945-44fa-841d-34cc6e0d1d20'
const AGENT_D_ACCUEIL_SLUG = 'agent-d-accueil'

// Permissions
/**
 * Organisation must be the same as current users's
 */
const SAME_ORGANISATION = { id: { _eq: '$CURRENT_USER.organisation' } }

/**
 * User must be in the same organisation
 */
const USER_IN_SAME_ORGANISATION = { organisation: SAME_ORGANISATION }

/**
 * Beneficiary must be in the same organisation
 @deprecated
 */
const DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION = {
  agent: USER_IN_SAME_ORGANISATION
}

/**
 * Relative must be in the same organisation
 @deprecated
 */
const DEPRECATED_RELATIVE_IN_SAME_ORGANISATION = {
  relative_beneficiary: DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION
}

const CREATED_BY_CURRENT_USER = {
  user_created: { id: { _eq: '$CURRENT_USER' } }
}

const BENEFICIARY_AGENT_IN_SAME_ORGANISATION = {
  referents: { referent: USER_IN_SAME_ORGANISATION }
}

const RELATIVE_IN_SAME_ORGANISATION = {
  relative_beneficiary: BENEFICIARY_AGENT_IN_SAME_ORGANISATION
}

const BENEFICIARY_REFERENT = {
  referents: { referent: { id: { _eq: '$CURRENT_USER' } } }
}

const PERMISSIONS_ID_SEQ = 'public.directus_permissions_id_seq'

const SEND_EMAIL_OPERATION_ID = 'abe1d091-1a02-47e9-9fe3-7d4fcd844d47'

const DUE_SYNTHESES_D_ENTRETIEN_TRIGGER_FLOW_OPERATION_ID =
  '06ba6a0b-44db-4294-8e7e-9ad49793d084'

const DUE_DEMANDES_D_AIDES_TRIGGER_FLOW_OPERATION_ID =
  '267e5cf3-ffca-47ab-8814-69ec48531203'

const DUE_SYNTHESES_D_ENTRETIEN_NOTIFICATIONS_FLOW_ID =
  '2c2c21cd-9368-4e11-9a1a-58838f95f777'

const DUE_DEMANDES_D_AIDES_NOTIFICATIONS_FLOW_ID =
  '026e4957-abb4-4a4e-8862-8921d36699c0'

const CREATE_DUE_DATE_NOTIFICATION_OPERATION_ID =
  'b201ecdf-35fa-451a-b46c-4039bf0588c8'

const DUE_TODAY_DEMANDES_D_AIDES_OPERATION_ID =
  'b60347b8-0c95-4032-ad52-1a9e8a043958'

const CREATE_HANDLING_DATE_NOTIFICATION_OPERATION_ID =
  'c1997cd0-32eb-426c-b061-6354b0947f84'

const DUE_TODAY_SYNTHESES_D_ENTRETIEN_OPERATION_ID =
  'c710c7d2-cffa-464b-86c6-030da1d99198'

module.exports = {
  RESPONSABLE_STRUCTURE_ID,
  TRAVAILLEUR_SOCIAL_ID,
  REFERENT_ID,
  INSTRUCTEUR_ID,
  AGENT_D_ACCUEIL_ID,
  SAME_ORGANISATION,
  USER_IN_SAME_ORGANISATION,
  DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION,
  DEPRECATED_RELATIVE_IN_SAME_ORGANISATION,
  CREATED_BY_CURRENT_USER,
  BENEFICIARY_AGENT_IN_SAME_ORGANISATION,
  RELATIVE_IN_SAME_ORGANISATION,
  PERMISSIONS_ID_SEQ,
  BENEFICIARY_REFERENT,
  SEND_EMAIL_OPERATION_ID,
  RESPONSABLE_STRUCTURE_SLUG,
  TRAVAILLEUR_SOCIAL_SLUG,
  REFERENT_SLUG,
  INSTRUCTEUR_SLUG,
  AGENT_D_ACCUEIL_SLUG,
  DUE_SYNTHESES_D_ENTRETIEN_TRIGGER_FLOW_OPERATION_ID,
  DUE_DEMANDES_D_AIDES_TRIGGER_FLOW_OPERATION_ID,
  DUE_SYNTHESES_D_ENTRETIEN_NOTIFICATIONS_FLOW_ID,
  DUE_DEMANDES_D_AIDES_NOTIFICATIONS_FLOW_ID,
  CREATE_DUE_DATE_NOTIFICATION_OPERATION_ID,
  DUE_TODAY_DEMANDES_D_AIDES_OPERATION_ID,
  CREATE_HANDLING_DATE_NOTIFICATION_OPERATION_ID,
  DUE_TODAY_SYNTHESES_D_ENTRETIEN_OPERATION_ID
}
