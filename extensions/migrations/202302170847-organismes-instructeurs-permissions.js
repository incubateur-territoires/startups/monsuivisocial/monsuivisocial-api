const {
  AGENT_D_ACCUEIL_ID,
  RESPONSABLE_STRUCTURE_ID,
  REFERENT_ID,
  INSTRUCTEUR_ID,
  TRAVAILLEUR_SOCIAL_ID,
  USER_IN_SAME_ORGANISATION
} = require('../../utils/constants')
const { insertPermission } = require('../../utils')

module.exports = {
  async up(knex) {
    await insertPermission(
      knex,
      AGENT_D_ACCUEIL_ID,
      'organismes_instructeurs',
      'read',
      { _and: [USER_IN_SAME_ORGANISATION] }
    )

    await insertPermission(
      knex,
      RESPONSABLE_STRUCTURE_ID,
      'organismes_instructeurs',
      'create',
      {},
      '*'
    )

    await insertPermission(
      knex,
      RESPONSABLE_STRUCTURE_ID,
      'organismes_instructeurs',
      'delete',
      { _and: [USER_IN_SAME_ORGANISATION] }
    )

    await insertPermission(
      knex,
      RESPONSABLE_STRUCTURE_ID,
      'organismes_instructeurs',
      'read',
      { _and: [USER_IN_SAME_ORGANISATION] },
      '*'
    )

    await insertPermission(
      knex,
      RESPONSABLE_STRUCTURE_ID,
      'organismes_instructeurs',
      'update',
      { _and: [USER_IN_SAME_ORGANISATION] },
      '*'
    )

    await insertPermission(
      knex,
      REFERENT_ID,
      'organismes_instructeurs',
      'create',
      {},
      '*'
    )

    await insertPermission(
      knex,
      REFERENT_ID,
      'organismes_instructeurs',
      'read',
      { _and: [USER_IN_SAME_ORGANISATION] },
      '*'
    )

    await insertPermission(
      knex,
      REFERENT_ID,
      'organismes_instructeurs',
      'update',
      { _and: [USER_IN_SAME_ORGANISATION] },
      '*'
    )

    await insertPermission(
      knex,
      INSTRUCTEUR_ID,
      'organismes_instructeurs',
      'create',
      {},
      '*'
    )

    await insertPermission(
      knex,
      INSTRUCTEUR_ID,
      'organismes_instructeurs',
      'read',
      { _and: [USER_IN_SAME_ORGANISATION] },
      '*'
    )

    await insertPermission(
      knex,
      INSTRUCTEUR_ID,
      'organismes_instructeurs',
      'update',
      { _and: [USER_IN_SAME_ORGANISATION] },
      '*'
    )

    await insertPermission(
      knex,
      TRAVAILLEUR_SOCIAL_ID,
      'organismes_instructeurs',
      'create',
      {},
      '*'
    )

    await insertPermission(
      knex,
      TRAVAILLEUR_SOCIAL_ID,
      'organismes_instructeurs',
      'read',
      { _and: [USER_IN_SAME_ORGANISATION] },
      '*'
    )

    await insertPermission(
      knex,
      TRAVAILLEUR_SOCIAL_ID,
      'organismes_instructeurs',
      'update',
      { _and: [USER_IN_SAME_ORGANISATION] },
      '*'
    )
  },

  async down() {}
}
