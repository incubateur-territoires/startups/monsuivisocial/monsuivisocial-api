const { insertPermission } = require('../../utils')
const {
  SAME_ORGANISATION,
  CREATED_BY_CURRENT_USER,
  USER_IN_SAME_ORGANISATION,
  INSTRUCTEUR_ID,
  BENEFICIARY_AGENT_IN_SAME_ORGANISATION,
  RELATIVE_IN_SAME_ORGANISATION
} = require('../../utils/constants')

module.exports = {
  async up(knex) {
    await createInstructeurRoleAndPermissions(knex)
  },

  async down(knex) {
    await deleteInstructeurRoleAndPermissions(knex)
  }
}

async function createInstructeurRoleAndPermissions(knex) {
  await knex('directus_roles').insert({
    id: INSTRUCTEUR_ID,
    name: 'Instructeur',
    description:
      'Peut gérer tous les bénéficiaires de sa structure, mais a des droits de suppression de donnée plus restreint.',
    enforce_tfa: false,
    admin_access: false,
    app_access: false
  })

  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'directus_roles',
    'read',
    {},
    '*'
  )

  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'directus_users',
    'read',
    { _and: [USER_IN_SAME_ORGANISATION] },
    'id,first_name,last_name,email,role,organisation'
  )

  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'directus_revisions',
    'read',
    {},
    '*'
  )

  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'directus_activity',
    'read',
    {},
    '*'
  )

  await createInstructeurBeneficiaryPermissions(knex)

  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'beneficiary_referents',
    'create',
    {},
    '*'
  )
  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'beneficiary_referents',
    'read',
    { _and: [{ referent: USER_IN_SAME_ORGANISATION }] },
    '*'
  )
  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'beneficiary_referents',
    'delete',
    { _and: [{ referent: USER_IN_SAME_ORGANISATION }] }
  )

  await createInstructeurDirectusFilesPermissions(knex)
  await createInstructeurRelativePermissions(knex)
  await createInstructeurFollowUpsPermissions(knex)
  await createInstructeurPrivateFollowUpsSynthesisPermissions(knex)
  await createInstructeurFollowUpsFollowUpTypesPermissions(knex)

  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'follow_up_types',
    'read',
    {
      _and: [{ organisations: { organisation_id: SAME_ORGANISATION } }]
    },
    '*'
  )

  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'directus_folders',
    'read',
    {
      _and: [{ name: { _contains: 'Beneficiary documents' } }]
    },
    '*'
  )

  await createInstructeurHelpRequestsPermissions(knex)
  await createInstructeurPrivateHelpRequestsSynthesisPermissions(knex)

  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'organisation',
    'read',
    { _and: [SAME_ORGANISATION] },
    '*'
  )

  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'organisation_follow_up_types',
    'read',
    { _and: [{ organisation_id: SAME_ORGANISATION }] },
    '*'
  )

  await createInstructeurOrganismesPrescripteursPermissions(knex)
}

async function createInstructeurBeneficiaryPermissions(knex) {
  await insertPermission(knex, INSTRUCTEUR_ID, 'beneficiary', 'create', {}, '*')
  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'beneficiary',
    'read',
    { _and: [BENEFICIARY_AGENT_IN_SAME_ORGANISATION] },
    '*'
  )
  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'beneficiary',
    'update',
    { _and: [BENEFICIARY_AGENT_IN_SAME_ORGANISATION] },
    '*'
  )
}

async function createInstructeurRelativePermissions(knex) {
  await insertPermission(knex, INSTRUCTEUR_ID, 'relatives', 'create', {}, '*')
  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'relatives',
    'read',
    { _and: [RELATIVE_IN_SAME_ORGANISATION] },
    '*'
  )
  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'relatives',
    'update',
    { _and: [RELATIVE_IN_SAME_ORGANISATION] },
    '*'
  )
  await insertPermission(knex, INSTRUCTEUR_ID, 'relatives', 'delete', {
    _and: [RELATIVE_IN_SAME_ORGANISATION]
  })
}

async function createInstructeurDirectusFilesPermissions(knex) {
  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'directus_files',
    'create',
    {},
    '*'
  )
  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'directus_files',
    'read',
    { _and: [{ beneficiary: BENEFICIARY_AGENT_IN_SAME_ORGANISATION }] },
    '*'
  )
  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'directus_files',
    'update',
    { _and: [{ beneficiary: BENEFICIARY_AGENT_IN_SAME_ORGANISATION }] },
    '*'
  )
}

async function createInstructeurFollowUpsPermissions(knex) {
  await insertPermission(knex, INSTRUCTEUR_ID, 'follow_ups', 'create', {}, '*')
  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'follow_ups',
    'read',
    { _and: [{ beneficiary: BENEFICIARY_AGENT_IN_SAME_ORGANISATION }] },
    '*'
  )
  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'follow_ups',
    'update',
    { _and: [CREATED_BY_CURRENT_USER] },
    '*'
  )
  await insertPermission(knex, INSTRUCTEUR_ID, 'follow_ups', 'delete', {
    _and: [CREATED_BY_CURRENT_USER]
  })
}

async function createInstructeurPrivateFollowUpsSynthesisPermissions(knex) {
  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'private_follow_up_synthesis',
    'create',
    {},
    '*'
  )
  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'private_follow_up_synthesis',
    'read',
    { _and: [{ follow_up: CREATED_BY_CURRENT_USER }] },
    '*'
  )
  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'private_follow_up_synthesis',
    'update',
    { _and: [{ follow_up: CREATED_BY_CURRENT_USER }] },
    '*'
  )
}

async function createInstructeurFollowUpsFollowUpTypesPermissions(knex) {
  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'follow_ups_follow_up_types',
    'create',
    {},
    '*'
  )
  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'follow_ups_follow_up_types',
    'read',
    {
      _and: [
        {
          follow_ups_id: { beneficiary: BENEFICIARY_AGENT_IN_SAME_ORGANISATION }
        }
      ]
    },
    '*'
  )
  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'follow_ups_follow_up_types',
    'delete',
    { _and: [{ follow_ups_id: CREATED_BY_CURRENT_USER }] }
  )
}

async function createInstructeurHelpRequestsPermissions(knex) {
  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'help_requests',
    'create',
    {},
    '*'
  )
  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'help_requests',
    'read',
    { _and: [{ beneficiary: BENEFICIARY_AGENT_IN_SAME_ORGANISATION }] },
    '*'
  )
  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'help_requests',
    'update',
    { _and: [{ beneficiary: BENEFICIARY_AGENT_IN_SAME_ORGANISATION }] },
    '*'
  )
  await insertPermission(knex, INSTRUCTEUR_ID, 'help_requests', 'delete', {
    _and: [CREATED_BY_CURRENT_USER]
  })
}

async function createInstructeurPrivateHelpRequestsSynthesisPermissions(knex) {
  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'private_help_request_synthesis',
    'create',
    {},
    '*'
  )
  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'private_help_request_synthesis',
    'read',
    { _and: [{ help_request: CREATED_BY_CURRENT_USER }] },
    '*'
  )
  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'private_help_request_synthesis',
    'update',
    { _and: [{ help_request: CREATED_BY_CURRENT_USER }] },
    '*'
  )
}

async function createInstructeurOrganismesPrescripteursPermissions(knex) {
  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'organismes_prescripteurs',
    'create',
    {},
    '*'
  )
  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'organismes_prescripteurs',
    'read',
    { _and: [{ organisation: SAME_ORGANISATION }] },
    '*'
  )
  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'organismes_prescripteurs',
    'update',
    { _and: [{ organisation: SAME_ORGANISATION }] },
    '*'
  )
}

async function deleteInstructeurRoleAndPermissions(knex) {
  await knex('directus_roles').where({ id: INSTRUCTEUR_ID }).del()
}
