module.exports = {
  async up(knex) {
    await knex('directus_flows')
      .where({
        id: 'b7164560-b929-4111-b53d-9f8af1c040fe'
      })
      .update({ operation: 'aa6faf9a-34fb-4b34-801d-4d33b6a18820' })

    await knex('directus_operations')
      .where({
        id: '5a8bc9df-a9ed-4d77-8e0b-ffe37a1503db'
      })
      .del()

    await knex('directus_operations')
      .where({
        id: 'aa6faf9a-34fb-4b34-801d-4d33b6a18820'
      })
      .update({
        name: 'Sélectionne les utilisateurs Inclusion Connect',
        key: 'inclusion_connect_user',
        position_x: 19,
        position_y: 1,
        resolve: 'abe1d091-1a02-47e9-9fe3-7d4fcd844d47'
      })
  },

  async down(knex) {}
}
