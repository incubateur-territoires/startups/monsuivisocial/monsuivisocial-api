const {
  AGENT_D_ACCUEIL_ID,
  RESPONSABLE_STRUCTURE_ID,
  TRAVAILLEUR_SOCIAL_ID,
  INSTRUCTEUR_ID,
  BENEFICIARY_AGENT_IN_SAME_ORGANISATION
} = require('../../utils/constants')

module.exports = {
  async up(knex) {
    await knex('directus_permissions')
      .whereIn('role', [
        RESPONSABLE_STRUCTURE_ID,
        TRAVAILLEUR_SOCIAL_ID,
        INSTRUCTEUR_ID,
        AGENT_D_ACCUEIL_ID
      ])
      .andWhere({
        collection: 'comments',
        action: 'read'
      })
      .update({
        permissions: {
          _and: [
            {
              _or: [
                {
                  _and: [
                    {
                      help_request: {
                        beneficiary: BENEFICIARY_AGENT_IN_SAME_ORGANISATION
                      }
                    },
                    { interview: { _null: true } }
                  ]
                },
                {
                  _and: [
                    {
                      interview: {
                        beneficiary: BENEFICIARY_AGENT_IN_SAME_ORGANISATION
                      }
                    },
                    { help_request: { _null: true } }
                  ]
                }
              ]
            }
          ]
        }
      })
  },

  async down() {}
}
