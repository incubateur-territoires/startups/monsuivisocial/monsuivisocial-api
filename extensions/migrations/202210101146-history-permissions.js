const {
  RESPONSABLE_STRUCTURE_ID,
  TRAVAILLEUR_SOCIAL_ID
} = require('../../utils/constants')
const { setSequenceValue } = require('../../utils')

module.exports = {
  async up(knex) {
    await knex('directus_permissions').insert([
      {
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'directus_revisions',
        action: 'read',
        permissions: {},
        fields: '*'
      },
      {
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'directus_activity',
        action: 'read',
        permissions: {},
        fields: '*'
      },
      {
        role: TRAVAILLEUR_SOCIAL_ID,
        collection: 'directus_revisions',
        action: 'read',
        permissions: {},
        fields: '*'
      },
      {
        role: TRAVAILLEUR_SOCIAL_ID,
        collection: 'directus_activity',
        action: 'read',
        permissions: {},
        fields: '*'
      }
    ])

    await setSequenceValue(knex, 'public.directus_permissions_id_seq', 86)
  },

  async down(knex) {
    await knex('directus_permissions')
      .whereIn('collection', ['directus_revisions', 'directus_activity'])
      .del()

    await setSequenceValue(knex, 'public.directus_permissions_id_seq', 82)
  }
}
