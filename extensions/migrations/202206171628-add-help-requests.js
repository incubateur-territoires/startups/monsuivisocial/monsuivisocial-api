const {
  RESPONSABLE_STRUCTURE_ID,
  TRAVAILLEUR_SOCIAL_ID,
  DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION,
  CREATED_BY_CURRENT_USER
} = require('../../utils/constants')
const { setSequenceValue } = require('../../utils')

module.exports = {
  async up(knex) {
    await knex('directus_permissions').insert([
      {
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'help_requests',
        action: 'create',
        permissions: {},
        fields: '*'
      },
      {
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'help_requests',
        action: 'read',
        permissions: {
          _and: [
            { beneficiary: DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION }
          ]
        },
        fields: '*'
      },
      {
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'help_requests',
        action: 'update',
        permissions: {
          _and: [
            { beneficiary: DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION }
          ]
        },
        fields: '*'
      },
      {
        role: TRAVAILLEUR_SOCIAL_ID,
        collection: 'help_requests',
        action: 'create',
        permissions: {},
        fields: '*'
      },
      {
        role: TRAVAILLEUR_SOCIAL_ID,
        collection: 'help_requests',
        action: 'read',
        permissions: {
          _and: [
            { beneficiary: DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION }
          ]
        },
        fields: '*'
      },
      {
        role: TRAVAILLEUR_SOCIAL_ID,
        collection: 'help_requests',
        action: 'update',
        permissions: {
          _and: [
            { beneficiary: DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION }
          ]
        },
        fields: '*'
      },
      {
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'private_help_request_synthesis',
        action: 'create',
        permissions: {},
        fields: '*'
      },
      {
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'private_help_request_synthesis',
        action: 'read',
        permissions: {
          _and: [{ help_request: CREATED_BY_CURRENT_USER }]
        },
        fields: '*'
      },
      {
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'private_help_request_synthesis',
        action: 'update',
        permissions: {
          _and: [{ help_request: CREATED_BY_CURRENT_USER }]
        },
        fields: '*'
      },
      {
        role: TRAVAILLEUR_SOCIAL_ID,
        collection: 'private_help_request_synthesis',
        action: 'create',
        permissions: {},
        fields: '*'
      },
      {
        role: TRAVAILLEUR_SOCIAL_ID,
        collection: 'private_help_request_synthesis',
        action: 'read',
        permissions: { _and: [{ help_request: CREATED_BY_CURRENT_USER }] },
        fields: '*'
      },
      {
        role: TRAVAILLEUR_SOCIAL_ID,
        collection: 'private_help_request_synthesis',
        action: 'update',
        permissions: { _and: [{ help_request: CREATED_BY_CURRENT_USER }] },
        fields: '*'
      }
    ])

    await setSequenceValue(knex, 'public.directus_permissions_id_seq', 76)
  },

  async down(knex) {
    await knex('directus_permissions')
      .whereIn('collection', [
        'help_requests',
        'private_help_request_synthesis'
      ])
      .del()

    await setSequenceValue(knex, 'public.directus_permissions_id_seq', 64)
  }
}
