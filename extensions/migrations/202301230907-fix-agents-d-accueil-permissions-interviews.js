const {
  AGENT_D_ACCUEIL_ID,
  CREATED_BY_CURRENT_USER
} = require('../../utils/constants')
const { insertPermission } = require('../../utils')

module.exports = {
  async up(knex) {
    await knex('directus_permissions')
      .where({
        role: AGENT_D_ACCUEIL_ID,
        collection: 'follow_ups',
        action: 'read'
      })
      .update({
        fields:
          'id,user_created,date_created,user_updated,date_updated,beneficiary,type,date,follow_up_types,synthesis,help_request,organisation_name,place,redirected,third_person_name,due_date,classified,forwarded_to_justice,interventions,ministre,numero_pegase,signalements,status,documents,comments,organisme_prescripteur,first_follow_up'
      })

    await knex('directus_permissions')
      .where({
        role: AGENT_D_ACCUEIL_ID,
        collection: 'follow_ups',
        action: 'update'
      })
      .update({
        fields:
          'id,user_created,date_created,user_updated,date_updated,beneficiary,type,date,follow_up_types,synthesis,status,help_request,organisation_name,place,redirected,third_person_name,due_date,classified,forwarded_to_justice,interventions,ministre,numero_pegase,signalements,organisme_prescripteur,first_follow_up,documents,comments'
      })

    await insertPermission(
      knex,
      AGENT_D_ACCUEIL_ID,
      'follow_ups_directus_files',
      'create',
      {},
      '*'
    )

    await insertPermission(
      knex,
      AGENT_D_ACCUEIL_ID,
      'follow_ups_directus_files',
      'delete',
      { _and: [{ follow_up: CREATED_BY_CURRENT_USER }] }
    )

    await insertPermission(
      knex,
      AGENT_D_ACCUEIL_ID,
      'follow_ups_directus_files',
      'read',
      {
        _and: [
          { follow_up: CREATED_BY_CURRENT_USER },
          { directus_file: { uploaded_by: { id: { _eq: '$CURRENT_USER' } } } }
        ]
      },
      'id,follow_up,directus_file'
    )

    await knex('directus_permissions')
      .where({
        role: AGENT_D_ACCUEIL_ID,
        collection: 'help_requests',
        action: 'read'
      })
      .update({
        fields:
          'follow_up_types,follow_up_type,beneficiary,id,opening_date,financial_support,external_organisation,status,examination_date,decision_date,handling_date,dispatch_date,user_created,examining_organisation,due_date,full_file'
      })
  },

  async down() {}
}
