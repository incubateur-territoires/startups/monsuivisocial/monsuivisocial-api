const { RESPONSABLE_STRUCTURE_ID } = require('../../utils/constants')

module.exports = {
  async up(knex) {
    await knex('directus_roles')
      .where({ id: RESPONSABLE_STRUCTURE_ID })
      .update({
        app_access: false
      })
  },

  async down() {}
}
