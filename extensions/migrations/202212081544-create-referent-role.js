const { insertPermission } = require('../../utils')
const {
  REFERENT_ID,
  SAME_ORGANISATION,
  CREATED_BY_CURRENT_USER,
  BENEFICIARY_REFERENT,
  USER_IN_SAME_ORGANISATION
} = require('../../utils/constants')

module.exports = {
  async up(knex) {
    await createReferentRoleAndPermissions(knex)
  },

  async down(knex) {
    await deleteReferentRoleAndPermissions(knex)
  }
}

async function createReferentRoleAndPermissions(knex) {
  await knex('directus_roles').insert({
    id: REFERENT_ID,
    name: 'Référent',
    description:
      'Peut uniquement gérer les bénéficiaires dont il est référent.',
    enforce_tfa: false,
    admin_access: false,
    app_access: false
  })

  await insertPermission(knex, REFERENT_ID, 'directus_roles', 'read', {}, '*')

  await insertPermission(
    knex,
    REFERENT_ID,
    'directus_users',
    'read',
    { _and: [USER_IN_SAME_ORGANISATION] },
    'id,first_name,last_name,email,role,organisation'
  )

  await insertPermission(
    knex,
    REFERENT_ID,
    'directus_revisions',
    'read',
    {},
    '*'
  )

  await insertPermission(
    knex,
    REFERENT_ID,
    'directus_activity',
    'read',
    {},
    '*'
  )

  await createReferentBeneficiaryPermissions(knex)
  await createReferentBeneficiaryReferentsPermissions(knex)
  await createReferentDirectusFilesPermissions(knex)
  await createReferentRelativePermissions(knex)
  await createReferentFollowUpsPermissions(knex)
  await createReferentPrivateFollowUpsSynthesisPermissions(knex)
  await createReferentFollowUpsFollowUpTypesPermissions(knex)

  await insertPermission(
    knex,
    REFERENT_ID,
    'follow_up_types',
    'read',
    {
      _and: [{ organisations: { organisation_id: SAME_ORGANISATION } }]
    },
    '*'
  )

  await insertPermission(
    knex,
    REFERENT_ID,
    'directus_folders',
    'read',
    {
      _and: [{ name: { _contains: 'Beneficiary documents' } }]
    },
    '*'
  )

  await createReferentHelpRequestsPermissions(knex)
  await createReferentPrivateHelpRequestsSynthesisPermissions(knex)

  await insertPermission(
    knex,
    REFERENT_ID,
    'organisation',
    'read',
    { _and: [SAME_ORGANISATION] },
    '*'
  )

  await insertPermission(
    knex,
    REFERENT_ID,
    'organisation_follow_up_types',
    'read',
    { _and: [{ organisation_id: SAME_ORGANISATION }] },
    '*'
  )

  await createReferentOrganismesPrescripteursPermissions(knex)
}

async function createReferentBeneficiaryPermissions(knex) {
  await insertPermission(knex, REFERENT_ID, 'beneficiary', 'create', {}, '*')
  await insertPermission(
    knex,
    REFERENT_ID,
    'beneficiary',
    'read',
    { _and: [BENEFICIARY_REFERENT] },
    '*'
  )
  await insertPermission(
    knex,
    REFERENT_ID,
    'beneficiary',
    'update',
    { _and: [BENEFICIARY_REFERENT] },
    '*'
  )
}

async function createReferentBeneficiaryReferentsPermissions(knex) {
  await insertPermission(
    knex,
    REFERENT_ID,
    'beneficiary_referents',
    'create',
    {},
    '*'
  )
  await insertPermission(
    knex,
    REFERENT_ID,
    'beneficiary_referents',
    'read',
    { _and: [{ referent: USER_IN_SAME_ORGANISATION }] },
    '*'
  )

  await insertPermission(knex, REFERENT_ID, 'beneficiary_referents', 'delete', {
    _and: [
      { referent: USER_IN_SAME_ORGANISATION },
      { beneficiary: BENEFICIARY_REFERENT },
      { referent: { id: { _neq: '$CURRENT_USER' } } }
    ]
  })
}

async function createReferentRelativePermissions(knex) {
  await insertPermission(knex, REFERENT_ID, 'relatives', 'create', {}, '*')
  await insertPermission(
    knex,
    REFERENT_ID,
    'relatives',
    'read',
    { _and: [{ relative_beneficiary: BENEFICIARY_REFERENT }] },
    '*'
  )
  await insertPermission(
    knex,
    REFERENT_ID,
    'relatives',
    'update',
    { _and: [{ relative_beneficiary: BENEFICIARY_REFERENT }] },
    '*'
  )
  await insertPermission(knex, REFERENT_ID, 'relatives', 'delete', {
    _and: [{ relative_beneficiary: BENEFICIARY_REFERENT }]
  })
}

async function createReferentDirectusFilesPermissions(knex) {
  await insertPermission(knex, REFERENT_ID, 'directus_files', 'create', {}, '*')
  await insertPermission(
    knex,
    REFERENT_ID,
    'directus_files',
    'read',
    { _and: [{ beneficiary: BENEFICIARY_REFERENT }] },
    '*'
  )
  await insertPermission(
    knex,
    REFERENT_ID,
    'directus_files',
    'update',
    { _and: [{ beneficiary: BENEFICIARY_REFERENT }] },
    '*'
  )
  await insertPermission(knex, REFERENT_ID, 'directus_files', 'delete', {
    _and: [{ beneficiary: BENEFICIARY_REFERENT }]
  })
}

async function createReferentFollowUpsPermissions(knex) {
  await insertPermission(knex, REFERENT_ID, 'follow_ups', 'create', {}, '*')
  await insertPermission(
    knex,
    REFERENT_ID,
    'follow_ups',
    'read',
    { _and: [{ beneficiary: BENEFICIARY_REFERENT }] },
    '*'
  )
  await insertPermission(
    knex,
    REFERENT_ID,
    'follow_ups',
    'update',
    { _and: [CREATED_BY_CURRENT_USER] },
    '*'
  )
  await insertPermission(knex, REFERENT_ID, 'follow_ups', 'delete', {
    _and: [CREATED_BY_CURRENT_USER]
  })
}

async function createReferentPrivateFollowUpsSynthesisPermissions(knex) {
  await insertPermission(
    knex,
    REFERENT_ID,
    'private_follow_up_synthesis',
    'create',
    {},
    '*'
  )
  await insertPermission(
    knex,
    REFERENT_ID,
    'private_follow_up_synthesis',
    'read',
    { _and: [{ follow_up: CREATED_BY_CURRENT_USER }] },
    '*'
  )
  await insertPermission(
    knex,
    REFERENT_ID,
    'private_follow_up_synthesis',
    'update',
    { _and: [{ follow_up: CREATED_BY_CURRENT_USER }] },
    '*'
  )
}

async function createReferentFollowUpsFollowUpTypesPermissions(knex) {
  await insertPermission(
    knex,
    REFERENT_ID,
    'follow_ups_follow_up_types',
    'create',
    {},
    '*'
  )
  await insertPermission(
    knex,
    REFERENT_ID,
    'follow_ups_follow_up_types',
    'read',
    { _and: [{ follow_ups_id: { beneficiary: BENEFICIARY_REFERENT } }] },
    '*'
  )
  await insertPermission(
    knex,
    REFERENT_ID,
    'follow_ups_follow_up_types',
    'delete',
    { _and: [{ follow_ups_id: CREATED_BY_CURRENT_USER }] }
  )
}

async function createReferentHelpRequestsPermissions(knex) {
  await insertPermission(knex, REFERENT_ID, 'help_requests', 'create', {}, '*')
  await insertPermission(
    knex,
    REFERENT_ID,
    'help_requests',
    'read',
    { _and: [{ beneficiary: BENEFICIARY_REFERENT }] },
    '*'
  )
  await insertPermission(
    knex,
    REFERENT_ID,
    'help_requests',
    'update',
    { _and: [{ beneficiary: BENEFICIARY_REFERENT }] },
    '*'
  )
  await insertPermission(knex, REFERENT_ID, 'help_requests', 'delete', {
    _and: [CREATED_BY_CURRENT_USER]
  })
}

async function createReferentPrivateHelpRequestsSynthesisPermissions(knex) {
  await insertPermission(
    knex,
    REFERENT_ID,
    'private_help_request_synthesis',
    'create',
    {},
    '*'
  )
  await insertPermission(
    knex,
    REFERENT_ID,
    'private_help_request_synthesis',
    'read',
    { _and: [{ help_request: CREATED_BY_CURRENT_USER }] },
    '*'
  )
  await insertPermission(
    knex,
    REFERENT_ID,
    'private_help_request_synthesis',
    'update',
    { _and: [{ help_request: CREATED_BY_CURRENT_USER }] },
    '*'
  )
}

async function createReferentOrganismesPrescripteursPermissions(knex) {
  await insertPermission(
    knex,
    REFERENT_ID,
    'organismes_prescripteurs',
    'create',
    {},
    '*'
  )
  await insertPermission(
    knex,
    REFERENT_ID,
    'organismes_prescripteurs',
    'read',
    { _and: [{ organisation: SAME_ORGANISATION }] },
    '*'
  )
  await insertPermission(
    knex,
    REFERENT_ID,
    'organismes_prescripteurs',
    'update',
    { _and: [{ organisation: SAME_ORGANISATION }] },
    '*'
  )
}

async function deleteReferentRoleAndPermissions(knex) {
  await knex('directus_roles').where({ id: REFERENT_ID }).del()
}
