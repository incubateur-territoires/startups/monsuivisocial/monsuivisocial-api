const { AGENT_D_ACCUEIL_ID } = require('../../utils/constants')

module.exports = {
  async up(knex) {
    await knex('directus_permissions')
      .where({
        role: AGENT_D_ACCUEIL_ID,
        collection: 'directus_files',
        action: 'read'
      })
      .update({
        permissions: {
          _and: [{ uploaded_by: { id: { _eq: '$CURRENT_USER' } } }]
        }
      })

    await knex('directus_permissions')
      .where({
        role: AGENT_D_ACCUEIL_ID,
        collection: 'directus_files',
        action: 'update'
      })
      .del()

    await knex('directus_permissions')
      .where({
        role: AGENT_D_ACCUEIL_ID,
        collection: 'follow_ups',
        action: 'update'
      })
      .update({
        fields:
          'id,user_created,date_created,user_updated,date_updated,beneficiary,type,date,follow_up_types,synthesis,status,help_request,organisation_name,place,redirected,third_person_name,due_date,classified,forwarded_to_justice,interventions,ministre,numero_pegase,signalements,organismes_prescripteurs'
      })
  },

  async down() {}
}
