const {
  RESPONSABLE_STRUCTURE_ID,
  RESPONSABLE_STRUCTURE_SLUG,
  TRAVAILLEUR_SOCIAL_ID,
  TRAVAILLEUR_SOCIAL_SLUG,
  INSTRUCTEUR_ID,
  INSTRUCTEUR_SLUG,
  REFERENT_ID,
  REFERENT_SLUG,
  AGENT_D_ACCUEIL_ID,
  AGENT_D_ACCUEIL_SLUG
} = require('../../utils/constants')

module.exports = {
  async up(knex) {
    await knex('directus_roles')
      .where({ id: RESPONSABLE_STRUCTURE_ID })
      .update({ slug: RESPONSABLE_STRUCTURE_SLUG })

    await knex('directus_roles')
      .where({ id: TRAVAILLEUR_SOCIAL_ID })
      .update({ slug: TRAVAILLEUR_SOCIAL_SLUG })

    await knex('directus_roles')
      .where({ id: INSTRUCTEUR_ID })
      .update({ slug: INSTRUCTEUR_SLUG })

    await knex('directus_roles')
      .where({ id: REFERENT_ID })
      .update({ slug: REFERENT_SLUG })

    await knex('directus_roles')
      .where({ id: AGENT_D_ACCUEIL_ID })
      .update({ slug: AGENT_D_ACCUEIL_SLUG })
  },

  async down(knex) {}
}
