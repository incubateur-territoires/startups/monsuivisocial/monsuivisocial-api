const { AGENT_D_ACCUEIL_ID } = require('../../utils/constants')

module.exports = {
  async up(knex) {
    await knex('directus_permissions')
      .where({
        role: AGENT_D_ACCUEIL_ID,
        collection: 'beneficiary',
        action: 'create'
      })
      .update({
        fields:
          'id,user_created,date_created,user_updated,date_updated,aidant_connect_authorisation,accordion-x8aezq,status,title,usual_name,birth_name,firstname,birthdate,birth_place,deathdate,gender,nationality,accommodation_mode,accommodation_name,accommodation_additional_information,street,street_number,address_complement,zip_code,city,no_phone,phone_1,phone_2,email,family_situation,caregiver,minor_children,major_children,mobility,administration,minister,follow_ups,referents,file_number'
      })
  },

  async down() {}
}
