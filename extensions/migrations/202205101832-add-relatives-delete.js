const { setSequenceValue } = require('../../utils')
const {
  TRAVAILLEUR_SOCIAL_ID,
  RESPONSABLE_STRUCTURE_ID,
  DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION
} = require('../../utils/constants')

module.exports = {
  async up(knex) {
    await knex('directus_permissions').insert([
      {
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'relatives',
        action: 'delete',
        permissions: {
          _and: [
            {
              relative_beneficiary:
                DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION
            }
          ]
        }
      },
      {
        role: TRAVAILLEUR_SOCIAL_ID,
        collection: 'relatives',
        action: 'delete',
        permissions: {
          _and: [
            {
              relative_beneficiary:
                DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION
            }
          ]
        }
      }
    ])

    await setSequenceValue(knex, 'public.directus_permissions_id_seq', 42)
  },

  async down(knex) {
    await knex('directus_permissions')
      .where({
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'relatives',
        action: 'delete'
      })
      .del()
    await knex('directus_permissions')
      .where({
        role: TRAVAILLEUR_SOCIAL_ID,
        collection: 'relatives',
        action: 'delete'
      })
      .del()
    await setSequenceValue(knex, 'public.directus_permissions_id_seq', 40)
  }
}
