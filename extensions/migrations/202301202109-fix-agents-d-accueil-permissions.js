const {
  AGENT_D_ACCUEIL_ID,
  USER_IN_SAME_ORGANISATION
} = require('../../utils/constants')
const { insertPermission } = require('../../utils')

module.exports = {
  async up(knex) {
    await insertPermission(
      knex,
      AGENT_D_ACCUEIL_ID,
      'organismes_prescripteurs',
      'read',
      { _and: [USER_IN_SAME_ORGANISATION] },
      'id,user_created,date_created,name,date_updated,user_updated,organisation'
    )
  },

  async down() {}
}
