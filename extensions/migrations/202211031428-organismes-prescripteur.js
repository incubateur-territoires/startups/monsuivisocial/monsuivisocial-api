const {
  RESPONSABLE_STRUCTURE_ID,
  TRAVAILLEUR_SOCIAL_ID,
  SAME_ORGANISATION
} = require('../../utils/constants')
const { setSequenceValue } = require('../../utils')

module.exports = {
  async up(knex) {
    await knex('directus_permissions').insert([
      {
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'organismes_prescripteurs',
        action: 'create',
        permissions: {},
        fields: '*'
      },
      {
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'organismes_prescripteurs',
        action: 'read',
        permissions: {
          _and: [{ organisation: SAME_ORGANISATION }]
        },
        fields: '*'
      },
      {
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'organismes_prescripteurs',
        action: 'update',
        permissions: {
          _and: [{ organisation: SAME_ORGANISATION }]
        },
        fields: '*'
      },
      {
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'organismes_prescripteurs',
        action: 'delete',
        permissions: {
          _and: [{ organisation: SAME_ORGANISATION }]
        },
        fields: '*'
      },
      {
        role: TRAVAILLEUR_SOCIAL_ID,
        collection: 'organismes_prescripteurs',
        action: 'create',
        permissions: {},
        fields: '*'
      },
      {
        role: TRAVAILLEUR_SOCIAL_ID,
        collection: 'organismes_prescripteurs',
        action: 'read',
        permissions: {
          _and: [{ organisation: SAME_ORGANISATION }]
        },
        fields: '*'
      },
      {
        role: TRAVAILLEUR_SOCIAL_ID,
        collection: 'organismes_prescripteurs',
        action: 'update',
        permissions: {
          _and: [{ organisation: SAME_ORGANISATION }]
        },
        fields: '*'
      }
    ])

    await setSequenceValue(knex, 'public.directus_permissions_id_seq', 93)
  },

  async down(knex) {
    await knex('directus_permissions')
      .whereIn('collection', ['organismes_prescripteurs'])
      .del()

    await setSequenceValue(knex, 'public.directus_permissions_id_seq', 86)
  }
}
