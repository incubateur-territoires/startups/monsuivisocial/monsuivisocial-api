const {
  DUE_SYNTHESES_D_ENTRETIEN_TRIGGER_FLOW_OPERATION_ID,
  DUE_DEMANDES_D_AIDES_TRIGGER_FLOW_OPERATION_ID,
  DUE_SYNTHESES_D_ENTRETIEN_NOTIFICATIONS_FLOW_ID,
  DUE_DEMANDES_D_AIDES_NOTIFICATIONS_FLOW_ID,
  CREATE_DUE_DATE_NOTIFICATION_OPERATION_ID,
  DUE_TODAY_DEMANDES_D_AIDES_OPERATION_ID,
  CREATE_HANDLING_DATE_NOTIFICATION_OPERATION_ID,
  DUE_TODAY_SYNTHESES_D_ENTRETIEN_OPERATION_ID
} = require('../../utils/constants')

const ADD_SYNTHESES_D_ENTRETIEN_COLLECTION_OPERATION = {
  id: '516b3db9-8853-411b-a57f-b3449a30ddd9',
  name: "Add syntheses d'entretien collection",
  key: 'add_syntheses_d_entretien_collection',
  type: 'exec',
  position_x: 37,
  position_y: 1,
  options: {
    code: "module.exports = async function({ $last }) {\n\treturn $last.map(s => ({...s, collection: 'follow_ups'}))\n}"
  },
  resolve: DUE_SYNTHESES_D_ENTRETIEN_TRIGGER_FLOW_OPERATION_ID,
  flow: DUE_SYNTHESES_D_ENTRETIEN_NOTIFICATIONS_FLOW_ID
}

const ADD_DEMANDES_D_AIDE_COLLECTION_OPERATION = {
  id: '8aeac4e6-de25-49a7-a6b6-a598f569397c',
  name: "Add demandes d'aide collection",
  key: 'add_demandes_d_aide_collection',
  type: 'exec',
  position_x: 37,
  position_y: 1,
  options: {
    code: "module.exports = async function({ $last }) {\n\treturn $last.map(h => ({...h, collection: 'help_requests'}))\n}"
  },
  resolve: DUE_DEMANDES_D_AIDES_TRIGGER_FLOW_OPERATION_ID,
  flow: DUE_DEMANDES_D_AIDES_NOTIFICATIONS_FLOW_ID
}

module.exports = {
  async up(knex) {
    await knex('directus_operations')
      .where({ id: DUE_SYNTHESES_D_ENTRETIEN_TRIGGER_FLOW_OPERATION_ID })
      .update({ position_x: 55 })

    await knex('directus_operations')
      .where({ id: DUE_DEMANDES_D_AIDES_TRIGGER_FLOW_OPERATION_ID })
      .update({ position_x: 55 })

    await knex('directus_operations')
      .where({ id: CREATE_DUE_DATE_NOTIFICATION_OPERATION_ID })
      .update({
        options: {
          collection: 'notifications',
          payload: {
            type: 'due_date',
            recipient: '{{ $last.user_created }}',
            item: [
              { collection: '{{ $last.collection }}', item: '{{ $last.id }}' }
            ]
          }
        }
      })

    await knex('directus_operations')
      .where({ id: CREATE_HANDLING_DATE_NOTIFICATION_OPERATION_ID })
      .update({
        options: {
          collection: 'notifications',
          payload: {
            type: 'handling_date',
            recipient: '{{ $last.user_created }}',
            item: [{ collection: 'help_requests', item: '{{ $last.id }}' }]
          }
        }
      })

    // Firstly remove resolve value to prevent unknown foreign key error
    await knex('directus_operations')
      .where({ id: DUE_TODAY_DEMANDES_D_AIDES_OPERATION_ID })
      .update({ resolve: null })

    await knex('directus_operations')
      .where({ id: DUE_TODAY_SYNTHESES_D_ENTRETIEN_OPERATION_ID })
      .update({ resolve: null })

    // The insertion has to be done the latest to prevent duplicate key value violation on directus_operations_resolve_unique
    await knex('directus_operations').insert([
      ADD_SYNTHESES_D_ENTRETIEN_COLLECTION_OPERATION,
      ADD_DEMANDES_D_AIDE_COLLECTION_OPERATION
    ])

    await knex('directus_operations')
      .where({ id: DUE_TODAY_DEMANDES_D_AIDES_OPERATION_ID })
      .update({ resolve: ADD_DEMANDES_D_AIDE_COLLECTION_OPERATION.id })

    await knex('directus_operations')
      .where({ id: DUE_TODAY_SYNTHESES_D_ENTRETIEN_OPERATION_ID })
      .update({ resolve: ADD_SYNTHESES_D_ENTRETIEN_COLLECTION_OPERATION.id })
  },

  down() {}
}
