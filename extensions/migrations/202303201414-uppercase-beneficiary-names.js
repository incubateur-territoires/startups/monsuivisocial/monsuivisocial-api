module.exports = {
  async up(knex) {
    const beneficiaries = await knex('beneficiary')

    for (const {
      id,
      usual_name: usualName,
      birth_name: birthName,
      firstname
    } of beneficiaries) {
      const [firstnameFirstLetter, ...firstnameRest] = firstname || ''

      await knex('beneficiary')
        .where({ id })
        .update({
          usual_name: usualName ? usualName.toUpperCase() : usualName,
          birth_name: birthName ? birthName.toUpperCase() : birthName,
          firstname: firstnameFirstLetter
            ? `${firstnameFirstLetter.toUpperCase()}${firstnameRest.join('')}`
            : firstname
        })
    }
  },

  async down() {}
}
