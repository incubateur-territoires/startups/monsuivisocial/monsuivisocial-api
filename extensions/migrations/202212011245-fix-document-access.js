const {
  RESPONSABLE_STRUCTURE_ID,
  TRAVAILLEUR_SOCIAL_ID,
  BENEFICIARY_AGENT_IN_SAME_ORGANISATION,
  DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION
} = require('../../utils/constants')

module.exports = {
  async up(knex) {
    await knex('directus_permissions')
      .whereIn('role', [RESPONSABLE_STRUCTURE_ID, TRAVAILLEUR_SOCIAL_ID])
      .andWhere({ collection: 'directus_files' })
      .andWhereNot({ action: 'create' })
      .update({
        permissions: {
          _and: [{ beneficiary: BENEFICIARY_AGENT_IN_SAME_ORGANISATION }]
        }
      })

    await knex('directus_permissions')
      .whereIn('action', ['read', 'delete'])
      .andWhere({
        role: TRAVAILLEUR_SOCIAL_ID,
        collection: 'follow_ups_follow_up_types'
      })
      .update({
        permissions: {
          _and: [
            {
              follow_ups_id: {
                beneficiary: BENEFICIARY_AGENT_IN_SAME_ORGANISATION
              }
            }
          ]
        }
      })
  },

  async down(knex) {
    await knex('directus_permissions')
      .whereIn('role', [RESPONSABLE_STRUCTURE_ID, TRAVAILLEUR_SOCIAL_ID])
      .andWhere({ collection: 'directus_files' })
      .andWhereNot({ action: 'create' })
      .update({
        permissions: {
          _and: [
            { beneficiary: DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION }
          ]
        }
      })

    await knex('directus_permissions')
      .whereIn('action', ['read', 'delete'])
      .andWhere({
        role: TRAVAILLEUR_SOCIAL_ID,
        collection: 'follow_ups_follow_up_types'
      })
      .update({
        permissions: {
          _and: [
            {
              follow_ups_id: {
                beneficiary: DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION
              }
            }
          ]
        }
      })
  }
}
