const {
  TRAVAILLEUR_SOCIAL_ID,
  REFERENT_ID,
  INSTRUCTEUR_ID
} = require('../../utils/constants')

module.exports = {
  async up(knex) {
    await knex('directus_permissions')
      .whereIn('role', [REFERENT_ID, INSTRUCTEUR_ID, TRAVAILLEUR_SOCIAL_ID])
      .where({
        collection: 'follow_ups',
        action: 'delete'
      })
      .del()

    await knex('directus_permissions')
      .whereIn('role', [REFERENT_ID, INSTRUCTEUR_ID, TRAVAILLEUR_SOCIAL_ID])
      .where({
        collection: 'help_requests',
        action: 'delete'
      })
      .del()
  },

  async down() {}
}
