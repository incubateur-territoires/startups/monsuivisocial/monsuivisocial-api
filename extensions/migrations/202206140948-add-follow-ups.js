const {
  RESPONSABLE_STRUCTURE_ID,
  TRAVAILLEUR_SOCIAL_ID,
  DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION,
  CREATED_BY_CURRENT_USER
} = require('../../utils/constants')
const { setSequenceValue } = require('../../utils')

module.exports = {
  async up(knex) {
    await knex('directus_permissions').insert([
      {
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'follow_ups',
        action: 'create',
        permissions: {},
        fields: '*'
      },
      {
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'follow_ups',
        action: 'read',
        permissions: {
          _and: [
            { beneficiary: DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION }
          ]
        },
        fields: '*'
      },
      {
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'follow_ups',
        action: 'update',
        permissions: {
          _and: [
            { beneficiary: DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION }
          ]
        },
        fields: '*'
      },
      {
        role: TRAVAILLEUR_SOCIAL_ID,
        collection: 'follow_ups',
        action: 'create',
        permissions: {},
        fields: '*'
      },
      {
        role: TRAVAILLEUR_SOCIAL_ID,
        collection: 'follow_ups',
        action: 'read',
        permissions: {
          _and: [
            { beneficiary: DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION }
          ]
        },
        fields: '*'
      },
      {
        role: TRAVAILLEUR_SOCIAL_ID,
        collection: 'follow_ups',
        action: 'update',
        permissions: {
          _and: [
            { beneficiary: DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION }
          ]
        },
        fields: '*'
      },
      {
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'private_follow_up_synthesis',
        action: 'create',
        permissions: {},
        fields: '*'
      },
      {
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'private_follow_up_synthesis',
        action: 'read',
        permissions: {
          _and: [{ follow_up: CREATED_BY_CURRENT_USER }]
        },
        fields: '*'
      },
      {
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'private_follow_up_synthesis',
        action: 'update',
        permissions: {
          _and: [{ follow_up: CREATED_BY_CURRENT_USER }]
        },
        fields: '*'
      },
      {
        role: TRAVAILLEUR_SOCIAL_ID,
        collection: 'private_follow_up_synthesis',
        action: 'create',
        permissions: {},
        fields: '*'
      },
      {
        role: TRAVAILLEUR_SOCIAL_ID,
        collection: 'private_follow_up_synthesis',
        action: 'read',
        permissions: { _and: [{ follow_up: CREATED_BY_CURRENT_USER }] },
        fields: '*'
      },
      {
        role: TRAVAILLEUR_SOCIAL_ID,
        collection: 'private_follow_up_synthesis',
        action: 'update',
        permissions: { _and: [{ follow_up: CREATED_BY_CURRENT_USER }] },
        fields: '*'
      }
    ])

    await setSequenceValue(knex, 'public.directus_permissions_id_seq', 64)
  },

  async down(knex) {
    await knex('directus_permissions')
      .whereIn('collection', ['follow_ups', 'private_follow_up_synthesis'])
      .del()

    await setSequenceValue(knex, 'public.directus_permissions_id_seq', 52)
  }
}
