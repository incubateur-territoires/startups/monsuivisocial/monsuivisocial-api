const {
  REFERENT_ID,
  CREATED_BY_CURRENT_USER
} = require('../../utils/constants')

module.exports = {
  async up(knex) {
    await knex('directus_permissions')
      .where({
        role: REFERENT_ID,
        collection: 'help_requests',
        action: 'update'
      })
      .update({ permissions: { _and: [CREATED_BY_CURRENT_USER] } })
  },

  down() {}
}
