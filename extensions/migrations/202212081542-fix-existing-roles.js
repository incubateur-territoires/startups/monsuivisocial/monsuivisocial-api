const { insertPermission } = require('../../utils')
const {
  RESPONSABLE_STRUCTURE_ID,
  BENEFICIARY_AGENT_IN_SAME_ORGANISATION,
  TRAVAILLEUR_SOCIAL_ID,
  CREATED_BY_CURRENT_USER
} = require('../../utils/constants')

module.exports = {
  async up(knex) {
    await fixResponsableDeStructureRoleAndPermissions(knex)
    await fixAgentRoleAndPermissions(knex)
  },

  async down(knex) {
    await unfixResponsableDeStructurePermissions(knex)
    await unfixAgentRoleAndPermissions(knex)
  }
}

async function fixResponsableDeStructureRoleAndPermissions(knex) {
  await knex('directus_roles').where({ id: RESPONSABLE_STRUCTURE_ID }).update({
    description:
      "Peut gérer tous les bénéficiaires de sa structure. Peut gérer la structure, les types d'accompagnement qu'elle propose et ses agents."
  })

  await insertPermission(
    knex,
    RESPONSABLE_STRUCTURE_ID,
    'follow_ups',
    'delete',
    { _and: [{ beneficiary: BENEFICIARY_AGENT_IN_SAME_ORGANISATION }] }
  )
  await insertPermission(
    knex,
    RESPONSABLE_STRUCTURE_ID,
    'private_follow_up_synthesis',
    'delete',
    {
      _and: [
        { follow_up: { beneficiary: BENEFICIARY_AGENT_IN_SAME_ORGANISATION } }
      ]
    }
  )
  await insertPermission(
    knex,
    RESPONSABLE_STRUCTURE_ID,
    'help_requests',
    'delete',
    { _and: [{ beneficiary: BENEFICIARY_AGENT_IN_SAME_ORGANISATION }] }
  )
  await insertPermission(
    knex,
    RESPONSABLE_STRUCTURE_ID,
    'private_help_request_synthesis',
    'delete',
    {
      _and: [
        {
          help_request: {
            beneficiary: BENEFICIARY_AGENT_IN_SAME_ORGANISATION
          }
        }
      ]
    }
  )
}

async function fixAgentRoleAndPermissions(knex) {
  await knex('directus_roles').where({ id: TRAVAILLEUR_SOCIAL_ID }).update({
    name: 'Travailleur social',
    description: 'Peut gérer tous les bénéficiaires de sa structure.',
    app_access: false
  })

  await knex('directus_permissions')
    .where({
      role: TRAVAILLEUR_SOCIAL_ID,
      collection: 'directus_users',
      action: 'create'
    })
    .del()

  await knex('directus_permissions')
    .where({
      role: TRAVAILLEUR_SOCIAL_ID,
      collection: 'directus_users',
      action: 'read'
    })
    .update({
      fields: 'id,first_name,last_name,email,role,organisation'
    })

  const directusRolesPermission = await knex('directus_permissions').where({
    role: TRAVAILLEUR_SOCIAL_ID,
    collection: 'directus_roles',
    action: 'read'
  })
  if (directusRolesPermission.length) {
    await knex('directus_permissions')
      .where({
        role: TRAVAILLEUR_SOCIAL_ID,
        collection: 'directus_roles',
        action: 'read'
      })
      .update({
        permissions: {},
        fields: '*'
      })
  } else {
    await insertPermission(
      knex,
      TRAVAILLEUR_SOCIAL_ID,
      'directus_roles',
      'read',
      {},
      '*'
    )
  }

  await insertPermission(knex, TRAVAILLEUR_SOCIAL_ID, 'beneficiary', 'delete', {
    _and: [BENEFICIARY_AGENT_IN_SAME_ORGANISATION]
  })

  await insertPermission(knex, TRAVAILLEUR_SOCIAL_ID, 'follow_ups', 'delete', {
    _and: [CREATED_BY_CURRENT_USER]
  })

  await insertPermission(
    knex,
    TRAVAILLEUR_SOCIAL_ID,
    'help_requests',
    'delete',
    {
      _and: [CREATED_BY_CURRENT_USER]
    }
  )

  await knex('directus_permissions')
    .where({
      role: TRAVAILLEUR_SOCIAL_ID,
      collection: 'follow_ups',
      action: 'update'
    })
    .update({ permissions: { _and: [CREATED_BY_CURRENT_USER] } })

  await knex('directus_permissions')
    .where({
      role: TRAVAILLEUR_SOCIAL_ID,
      collection: 'follow_ups_follow_up_types',
      action: 'delete'
    })
    .update({ permissions: { follow_ups_id: CREATED_BY_CURRENT_USER } })
}

async function unfixResponsableDeStructurePermissions(knex) {
  await knex('directus_permissions')
    .whereIn('collection', [
      'follow_ups',
      'private_follow_up_synthesis',
      'help_requests',
      'private_help_request_synthesis'
    ])
    .andWhere({ role: RESPONSABLE_STRUCTURE_ID, action: 'delete' })
    .del()
}

async function unfixAgentRoleAndPermissions(knex) {
  await knex('directus_roles')
    .where({ id: TRAVAILLEUR_SOCIAL_ID })
    .update({ app_access: true })

  await knex('directus_permissions')
    .where({
      role: TRAVAILLEUR_SOCIAL_ID,
      collection: 'directus_roles',
      action: 'read'
    })
    .del()

  await knex('directus_permissions')
    .whereIn('collection', ['beneficiary', 'follow_ups', 'help_requests'])
    .andWhere({
      role: TRAVAILLEUR_SOCIAL_ID,
      action: 'delete'
    })
    .del()
}
