const { AGENT_D_ACCUEIL_ID } = require('../../utils/constants')

module.exports = {
  async up(knex) {
    await knex('directus_permissions')
      .where({
        role: AGENT_D_ACCUEIL_ID,
        collection: 'follow_ups',
        action: 'create'
      })
      .update({
        fields:
          'id,user_created,date_created,user_updated,date_updated,beneficiary,type,date,follow_up_types,synthesis,status,help_request,organisation_name,place,redirected,third_person_name,due_date,classified,forwarded_to_justice,interventions,ministre,numero_pegase,signalements,organismes_prescripteurs,documents,organisme_prescripteur,first_follow_up'
      })
  },

  async down() {}
}
