const { RESPONSABLE_STRUCTURE_ID } = require('../../utils/constants')

module.exports = {
  async up(knex) {
    await knex('directus_permissions')
      .where({
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'directus_users',
        action: 'update'
      })
      .update({
        fields:
          'first_name,last_name,email,password,organisation,role,status,aidant_connect_authorisation'
      })
    await knex('directus_permissions')
      .where({
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'directus_users',
        action: 'read'
      })
      .update({
        fields: 'organisation,aidant_connect_authorisation'
      })
  },

  async down() {}
}
