const {
  TRAVAILLEUR_SOCIAL_ID,
  CREATED_BY_CURRENT_USER
} = require('../../utils/constants')

module.exports = {
  async up(knex) {
    await knex('directus_permissions')
      .where({
        role: TRAVAILLEUR_SOCIAL_ID,
        collection: 'help_requests',
        action: 'delete'
      })
      .update({
        permissions: {
          _and: [CREATED_BY_CURRENT_USER]
        }
      })
  },

  async down() {}
}
