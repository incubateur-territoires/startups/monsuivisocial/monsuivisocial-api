const {
  TRAVAILLEUR_SOCIAL_ID,
  REFERENT_ID,
  INSTRUCTEUR_ID,
  AGENT_D_ACCUEIL_ID
} = require('../../utils/constants')

module.exports = {
  async up(knex) {
    await knex('directus_permissions')
      .whereIn('role', [
        TRAVAILLEUR_SOCIAL_ID,
        REFERENT_ID,
        INSTRUCTEUR_ID,
        AGENT_D_ACCUEIL_ID
      ])
      .andWhere({
        collection: 'directus_users',
        action: 'read'
      })
      .update({
        fields: 'id,first_name,last_name,email,role,organisation,status'
      })
  },

  async down() {}
}
