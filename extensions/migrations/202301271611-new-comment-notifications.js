const NEW_COMMENT_NOTIFICATIONS_FLOW_ID = '71886863-7d02-4263-a63f-7399f51455ab'

const CREATE_NEW_COMMENT_NOTIFICATION_OPERATION = {
  id: '69980113-b537-4fe7-b4e8-eba412f0efee',
  name: 'Create new comment notification',
  key: 'create_new_comment_notification',
  type: 'item-create',
  position_x: 55,
  position_y: 1,
  options: {
    collection: 'notifications',
    permissions: '$full',
    payload: {
      type: 'new_comment',
      recipient: '{{ $last.item_user_created }}',
      item: [{ item: '{{ $last.item }}', collection: '{{ $last.collection }}' }]
    }
  },
  flow: NEW_COMMENT_NOTIFICATIONS_FLOW_ID
}

const SIMPLIFY_DATA_STRUCTURE_OPERATION = {
  id: 'ed8d6568-f048-4604-9f13-c6a3b704569f',
  name: 'Simplify data structure',
  key: 'simplify_data_structure',
  type: 'exec',
  position_x: 37,
  position_y: 1,
  options: {
    code: "module.exports = async function({ $last }) {\n    const comment_user_created = $last.user_created\n    const collection = $last.interview ? 'follow_ups' : 'help_requests'\n    const item = $last.interview?.id ||$last.help_request?.id\n    const item_user_created = $last.interview?.user_created || $last.help_request?.user_created\n    if (comment_user_created === item_user_created) return null\n\treturn {\n        item,\n        collection,\n        comment_user_created,\n        item_user_created\n    };\n}"
  },
  resolve: CREATE_NEW_COMMENT_NOTIFICATION_OPERATION.id,
  flow: NEW_COMMENT_NOTIFICATIONS_FLOW_ID
}

const READ_ITEM_AUTHOR_OPERATION = {
  id: 'd92517e8-e8ae-4a10-9515-9a8df25d2e76',
  name: 'Read item author',
  key: 'read_item_author',
  type: 'item-read',
  position_x: 19,
  position_y: 1,
  options: {
    permissions: '$full',
    collection: 'comments',
    key: '{{ $trigger.key }}',
    query: {
      fields: [
        'user_created',
        'help_request.id',
        'help_request.user_created',
        'interview.id',
        'interview.user_created'
      ]
    }
  },
  resolve: SIMPLIFY_DATA_STRUCTURE_OPERATION.id,
  flow: NEW_COMMENT_NOTIFICATIONS_FLOW_ID
}

const NEW_COMMENT_NOTIFICATIONS_FLOW = {
  id: NEW_COMMENT_NOTIFICATIONS_FLOW_ID,
  name: 'New comment notifications',
  icon: 'bolt',
  status: 'active',
  trigger: 'event',
  accountability: 'all',
  options: {
    type: 'action',
    scope: ['items.create'],
    collections: ['comments']
  },
  operation: READ_ITEM_AUTHOR_OPERATION.id
}

module.exports = {
  async up(knex) {
    await knex('directus_flows').insert([NEW_COMMENT_NOTIFICATIONS_FLOW])

    await knex('directus_operations').insert([
      CREATE_NEW_COMMENT_NOTIFICATION_OPERATION,
      SIMPLIFY_DATA_STRUCTURE_OPERATION,
      READ_ITEM_AUTHOR_OPERATION
    ])
  },

  down() {}
}
