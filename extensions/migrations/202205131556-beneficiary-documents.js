const {
  RESPONSABLE_STRUCTURE_ID,
  TRAVAILLEUR_SOCIAL_ID,
  DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION
} = require('../../utils/constants')
const { randomUUID } = require('crypto')
const { setSequenceValue } = require('../../utils')

module.exports = {
  async up(knex) {
    await knex('directus_folders').insert([
      { id: randomUUID(), name: 'Beneficiary documents' }
    ])

    await knex('directus_permissions').insert([
      {
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'directus_files',
        action: 'create',
        permissions: {},
        fields: '*'
      },
      {
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'directus_files',
        action: 'read',
        permissions: {
          _and: [
            { beneficiary: DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION }
          ]
        },
        fields: '*'
      },
      {
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'directus_files',
        action: 'update',
        permissions: {
          _and: [
            { beneficiary: DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION }
          ]
        },
        fields: '*'
      },
      {
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'directus_files',
        action: 'delete',
        permissions: {
          _and: [
            { beneficiary: DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION }
          ]
        }
      },
      {
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'directus_folders',
        action: 'read',
        permissions: {
          _and: [{ name: { _contains: 'Beneficiary documents' } }]
        },
        fields: '*'
      },
      {
        role: TRAVAILLEUR_SOCIAL_ID,
        collection: 'directus_files',
        action: 'create',
        permissions: {},
        fields: '*'
      },
      {
        role: TRAVAILLEUR_SOCIAL_ID,
        collection: 'directus_files',
        action: 'read',
        permissions: {
          _and: [
            { beneficiary: DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION }
          ]
        },
        fields: '*'
      },
      {
        role: TRAVAILLEUR_SOCIAL_ID,
        collection: 'directus_files',
        action: 'update',
        permissions: {
          _and: [
            { beneficiary: DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION }
          ]
        },
        fields: '*'
      },
      {
        role: TRAVAILLEUR_SOCIAL_ID,
        collection: 'directus_files',
        action: 'delete',
        permissions: {
          _and: [
            { beneficiary: DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION }
          ]
        }
      },
      {
        role: TRAVAILLEUR_SOCIAL_ID,
        collection: 'directus_folders',
        action: 'read',
        permissions: {
          _and: [{ name: { _contains: 'Beneficiary documents' } }]
        },
        fields: '*'
      }
    ])

    await setSequenceValue(knex, 'public.directus_permissions_id_seq', 52)
  },

  async down(knex) {
    await knex('directus_folders')
      .where({ name: 'Beneficiary documents' })
      .del()

    await knex('directus_permissions')
      .whereIn('collection', ['directus_files', 'directus_folders'])
      .del()

    await setSequenceValue(knex, 'public.directus_permissions_id_seq', 42)
  }
}
