const { RESPONSABLE_STRUCTURE_ID } = require('../../utils/constants')

module.exports = {
  async up(knex) {
    await knex('directus_permissions')
      .where({
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'directus_users',
        action: 'create'
      })
      .update({
        fields:
          'id,first_name,last_name,email,role,language,theme,organisation,provider,external_identifier,status'
      })
    await knex('directus_permissions')
      .where({
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'directus_users',
        action: 'update'
      })
      .update({
        fields: 'first_name,last_name,email,password,organisation,role,status'
      })
  },

  async down() {}
}
