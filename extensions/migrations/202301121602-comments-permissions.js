const {
  AGENT_D_ACCUEIL_ID,
  RESPONSABLE_STRUCTURE_ID,
  TRAVAILLEUR_SOCIAL_ID,
  INSTRUCTEUR_ID,
  REFERENT_ID,
  BENEFICIARY_REFERENT,
  CREATED_BY_CURRENT_USER
} = require('../../utils/constants')
const { insertPermission } = require('../../utils')

module.exports = {
  async up(knex) {
    await insertPermission(
      knex,
      RESPONSABLE_STRUCTURE_ID,
      'comments',
      'create',
      {},
      '*'
    )
    await insertPermission(
      knex,
      RESPONSABLE_STRUCTURE_ID,
      'comments',
      'read',
      {},
      '*'
    )

    await insertPermission(
      knex,
      TRAVAILLEUR_SOCIAL_ID,
      'comments',
      'create',
      {},
      '*'
    )
    await insertPermission(
      knex,
      TRAVAILLEUR_SOCIAL_ID,
      'comments',
      'read',
      {},
      '*'
    )

    await insertPermission(knex, INSTRUCTEUR_ID, 'comments', 'create', {}, '*')
    await insertPermission(knex, INSTRUCTEUR_ID, 'comments', 'read', {}, '*')

    await insertPermission(
      knex,
      AGENT_D_ACCUEIL_ID,
      'comments',
      'create',
      {},
      '*'
    )
    await insertPermission(
      knex,
      AGENT_D_ACCUEIL_ID,
      'comments',
      'read',
      { _and: [{ interview: CREATED_BY_CURRENT_USER }] },
      '*'
    )

    await insertPermission(knex, REFERENT_ID, 'comments', 'create', {}, '*')
    await insertPermission(
      knex,
      REFERENT_ID,
      'comments',
      'read',
      {
        _and: [
          {
            _or: [
              {
                _and: [
                  { help_request: { beneficiary: BENEFICIARY_REFERENT } },
                  { interview: { _null: true } }
                ]
              },
              {
                _and: [
                  { interview: { beneficiary: BENEFICIARY_REFERENT } },
                  { help_request: { _null: true } }
                ]
              }
            ]
          }
        ]
      },
      '*'
    )
  },

  async down(knex) {
    await knex('directus_permissions').where({ collection: 'comments' }).del()
  }
}
