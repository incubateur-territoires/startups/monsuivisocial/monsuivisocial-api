const {
  CREATE_HANDLING_DATE_NOTIFICATION_OPERATION_ID
} = require('../../utils/constants')

const CREATE_HANDLING_DATE_NOTIFICATION_FLOW_ID =
  'ce11d1b0-8731-42a5-b4f6-f7e1a0546d2f'

const HANDLED_DEMANDES_D_AIDES_NOTIFICATIONS_FLOW_ID =
  '3465c58c-3e14-4a91-b950-f1f2c81c34a3'

const HANDLED_DEMANDES_D_AIDES_TRIGGER_FLOW_OPERATION = {
  id: '9c7eac48-a3cd-4351-9aff-b710e10d1acc',
  name: 'Trigger Flow',
  key: 'handled_demandes_d_aides_trigger_flow',
  type: 'trigger',
  position_x: 37,
  position_y: 1,
  options: {
    flow: CREATE_HANDLING_DATE_NOTIFICATION_FLOW_ID,
    payload: '{{ $last }}'
  },
  flow: HANDLED_DEMANDES_D_AIDES_NOTIFICATIONS_FLOW_ID
}

const HANDLED_TODAY_DEMANDES_D_AIDES_OPERATION = {
  id: '920f4cb3-4009-4a48-93c4-e688bf579e56',
  name: "Handled today demandes d'aide",
  key: 'handled_today_demandes_d_aides',
  type: 'item-read',
  position_x: 19,
  position_y: 1,
  options: {
    collection: 'help_requests',
    query: { filter: { handling_date: { _between: ['$NOW(-1 day)', '$NOW'] } } }
  },
  resolve: HANDLED_DEMANDES_D_AIDES_TRIGGER_FLOW_OPERATION.id,
  flow: HANDLED_DEMANDES_D_AIDES_NOTIFICATIONS_FLOW_ID
}

const HANDLED_DEMANDES_D_AIDES_NOTIFICATIONS_FLOW = {
  id: HANDLED_DEMANDES_D_AIDES_NOTIFICATIONS_FLOW_ID,
  name: "Handled demandes d'aide notifications",
  icon: 'bolt',
  status: 'active',
  trigger: 'schedule',
  accountability: 'all',
  options: { cron: '0 1 * * *' },
  operation: HANDLED_TODAY_DEMANDES_D_AIDES_OPERATION.id
}

const CREATE_HANDLING_DATE_NOTIFICATION_OPERATION = {
  id: CREATE_HANDLING_DATE_NOTIFICATION_OPERATION_ID,
  name: 'Create notification',
  key: 'create_handling_date_notification',
  type: 'item-create',
  position_x: 19,
  position_y: 1,
  options: {
    collection: 'notifications',
    payload: {
      type: 'handling_date',
      recipient: '{{ $last.user_created }}',
      item: [{ item: '{{ $last.id }}' }]
    }
  },
  flow: CREATE_HANDLING_DATE_NOTIFICATION_FLOW_ID
}

const CREATE_HANDLING_DATE_NOTIFICATION_FLOW = {
  id: CREATE_HANDLING_DATE_NOTIFICATION_FLOW_ID,
  name: 'Create handling date notification',
  icon: 'bolt',
  status: 'active',
  trigger: 'operation',
  accountability: 'all',
  options: { return: '$last' },
  operation: CREATE_HANDLING_DATE_NOTIFICATION_OPERATION.id
}

module.exports = {
  async up(knex) {
    await knex('directus_flows').insert([
      HANDLED_DEMANDES_D_AIDES_NOTIFICATIONS_FLOW,
      CREATE_HANDLING_DATE_NOTIFICATION_FLOW
    ])

    await knex('directus_operations').insert([
      HANDLED_DEMANDES_D_AIDES_TRIGGER_FLOW_OPERATION,
      HANDLED_TODAY_DEMANDES_D_AIDES_OPERATION,
      CREATE_HANDLING_DATE_NOTIFICATION_OPERATION
    ])
  },

  down() {}
}
