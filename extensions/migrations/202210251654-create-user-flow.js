const { SEND_EMAIL_OPERATION_ID } = require('../../utils/constants')

const CREATE_USER_FLOW_ID = 'b7164560-b929-4111-b53d-9f8af1c040fe'
const SEND_EMAIL_OPERATION = {
  id: SEND_EMAIL_OPERATION_ID,
  name: 'Send Email',
  key: 'mail_5dm7h',
  type: 'mail',
  position_x: 37,
  position_y: 1,
  options: {
    to: '{{$trigger.payload.email}}',
    subject: 'Bienvenue sur Mon Suivi Social',
    body: "Bonjour,\n\nVotre responsable de structure vous a créé un compte sur l'application Mon Suivi Social.\n\nPour s'authentifier, Mon Suivi Social utilise l'outil Inclusion Connect qui vous permettra d'accéder à plusieurs outils numériques de l'inclusion, en plus de Mon Suivi Social.\n\nAfin d'accéder à l'application, veuillez cliquer sur le lien ci-dessous pour créer votre compte Inclusion Connect.\n\n[Créer son compte Inclusion Connect](https://connect.inclusion.beta.gouv.fr/realms/inclusion-connect/login-actions/registration?client_id=monsuivisocial)\n\nL'équipe de Mon Suivi Social\n\n"
  },
  flow: CREATE_USER_FLOW_ID
}

const NO_ADMINISTRATORS_OPERATION = {
  id: '5a8bc9df-a9ed-4d77-8e0b-ffe37a1503db',
  name: 'Ne sélectionne pas les administrateurs',
  key: 'no_administors',
  type: 'condition',
  position_x: 19,
  position_y: 1,
  options: {
    filter: {
      $trigger: {
        payload: {
          role: {
            _in: [
              '77c48d8b-a130-4a49-a00a-53a564bb9dbe',
              'c622eb01-b206-4f50-8c8b-66b6b7c1bcee'
            ]
          }
        }
      }
    }
  },
  resolve: SEND_EMAIL_OPERATION.id,
  flow: CREATE_USER_FLOW_ID
}

const INCLUSION_CONNECT_USER_OPERATION = {
  id: 'aa6faf9a-34fb-4b34-801d-4d33b6a18820',
  name: '[Futur] Sélectionne les utilisateurs Inclusion Connect',
  key: 'future_inclusion_connect_user',
  type: 'condition',
  position_x: 17,
  position_y: 19,
  options: {
    filter: {
      _and: [
        { $trigger: { payload: { provider: { _eq: 'keycloak' } } } },
        { $trigger: { payload: { external_identifier: { _nnull: true } } } }
      ]
    }
  },
  flow: CREATE_USER_FLOW_ID
}

const CREATE_USER_FLOW = {
  id: CREATE_USER_FLOW_ID,
  name: "Création d'utilisateur SSO",
  icon: 'bolt',
  description:
    "Lorsqu'un nouvel utilisateur (hors administrateur) est créé, envoie un mail à son adresse mail avec les instructions et un lien vers la création de compte Inclusion Connect.",
  status: 'active',
  trigger: 'event',
  accountability: 'all',
  options: {
    type: 'action',
    scope: ['items.create'],
    collections: ['directus_users']
  },
  operation: NO_ADMINISTRATORS_OPERATION.id
}

module.exports = {
  async up(knex) {
    await knex('directus_flows').insert(CREATE_USER_FLOW)
    await knex('directus_operations').insert([
      SEND_EMAIL_OPERATION,
      NO_ADMINISTRATORS_OPERATION,
      INCLUSION_CONNECT_USER_OPERATION
    ])
  },

  async down(knex) {
    await knex('directus_flows').where({ id: CREATE_USER_FLOW.id }).del()
    await knex('directus_operations')
      .whereIn('id', [
        SEND_EMAIL_OPERATION.id,
        NO_ADMINISTRATORS_OPERATION.id,
        INCLUSION_CONNECT_USER_OPERATION.id
      ])
      .del()
  }
}
