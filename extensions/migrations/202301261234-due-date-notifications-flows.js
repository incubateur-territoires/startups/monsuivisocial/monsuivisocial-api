const {
  DUE_SYNTHESES_D_ENTRETIEN_TRIGGER_FLOW_OPERATION_ID,
  DUE_DEMANDES_D_AIDES_TRIGGER_FLOW_OPERATION_ID,
  DUE_SYNTHESES_D_ENTRETIEN_NOTIFICATIONS_FLOW_ID,
  DUE_DEMANDES_D_AIDES_NOTIFICATIONS_FLOW_ID,
  CREATE_DUE_DATE_NOTIFICATION_OPERATION_ID,
  DUE_TODAY_DEMANDES_D_AIDES_OPERATION_ID,
  DUE_TODAY_SYNTHESES_D_ENTRETIEN_OPERATION_ID
} = require('../../utils/constants')

const CREATE_DUE_DATE_NOTIFICATION_FLOW_ID =
  '39061350-b0fe-4a81-9741-38c976715541'

const DUE_SYNTHESES_D_ENTRETIEN_TRIGGER_FLOW_OPERATION = {
  id: DUE_SYNTHESES_D_ENTRETIEN_TRIGGER_FLOW_OPERATION_ID,
  name: 'Trigger Flow',
  key: 'due_syntheses_d_entretien_trigger_flow',
  type: 'trigger',
  position_x: 37,
  position_y: 1,
  options: {
    flow: CREATE_DUE_DATE_NOTIFICATION_FLOW_ID,
    payload: '{{ $last }}'
  },
  flow: DUE_SYNTHESES_D_ENTRETIEN_NOTIFICATIONS_FLOW_ID
}

const DUE_TODAY_SYNTHESES_D_ENTRETIEN_OPERATION = {
  id: DUE_TODAY_SYNTHESES_D_ENTRETIEN_OPERATION_ID,
  name: "Due today syntheses d'entretien",
  key: 'due_today_syntheses_d_entretien',
  type: 'item-read',
  position_x: 19,
  position_y: 1,
  options: {
    collection: 'follow_ups',
    query: { filter: { due_date: { _between: ['$NOW(-1 day)', '$NOW'] } } }
  },
  resolve: DUE_SYNTHESES_D_ENTRETIEN_TRIGGER_FLOW_OPERATION.id,
  flow: DUE_SYNTHESES_D_ENTRETIEN_NOTIFICATIONS_FLOW_ID
}

const DUE_SYNTHESES_D_ENTRETIEN_NOTIFICATIONS_FLOW = {
  id: DUE_SYNTHESES_D_ENTRETIEN_NOTIFICATIONS_FLOW_ID,
  name: "Due syntheses d'entretien notifications",
  icon: 'bolt',
  status: 'active',
  trigger: 'schedule',
  accountability: 'all',
  options: { cron: '0 1 * * *' },
  operation: DUE_TODAY_SYNTHESES_D_ENTRETIEN_OPERATION.id
}

const DUE_DEMANDES_D_AIDES_TRIGGER_FLOW_OPERATION = {
  id: DUE_DEMANDES_D_AIDES_TRIGGER_FLOW_OPERATION_ID,
  name: 'Trigger Flow',
  key: 'due_demandes_d_aides_trigger_flow',
  type: 'trigger',
  position_x: 37,
  position_y: 1,
  options: {
    flow: CREATE_DUE_DATE_NOTIFICATION_FLOW_ID,
    payload: '{{ $last }}'
  },
  flow: DUE_DEMANDES_D_AIDES_NOTIFICATIONS_FLOW_ID
}

const DUE_TODAY_DEMANDES_D_AIDES_OPERATION = {
  id: DUE_TODAY_DEMANDES_D_AIDES_OPERATION_ID,
  name: "Due today demandes d'aide",
  key: 'due_today_demandes_d_aides',
  type: 'item-read',
  position_x: 19,
  position_y: 1,
  options: {
    collection: 'help_requests',
    query: { filter: { due_date: { _between: ['$NOW(-1 day)', '$NOW'] } } }
  },
  resolve: DUE_DEMANDES_D_AIDES_TRIGGER_FLOW_OPERATION.id,
  flow: DUE_DEMANDES_D_AIDES_NOTIFICATIONS_FLOW_ID
}

const DUE_DEMANDES_D_AIDES_NOTIFICATIONS_FLOW = {
  id: DUE_DEMANDES_D_AIDES_NOTIFICATIONS_FLOW_ID,
  name: "Due demandes d'aide notifications",
  icon: 'bolt',
  status: 'active',
  trigger: 'schedule',
  accountability: 'all',
  options: { cron: '0 1 * * *' },
  operation: DUE_TODAY_DEMANDES_D_AIDES_OPERATION.id
}

const CREATE_DUE_DATE_NOTIFICATION_OPERATION = {
  id: CREATE_DUE_DATE_NOTIFICATION_OPERATION_ID,
  name: 'Create notification',
  key: 'create_due_date_notification',
  type: 'item-create',
  position_x: 19,
  position_y: 1,
  options: {
    collection: 'notifications',
    payload: {
      type: 'due_date',
      recipient: '{{ $last.user_created }}',
      item: [{ item: '{{ $last.id }}' }]
    }
  },
  flow: CREATE_DUE_DATE_NOTIFICATION_FLOW_ID
}

const CREATE_DUE_DATE_NOTIFICATION_FLOW = {
  id: CREATE_DUE_DATE_NOTIFICATION_FLOW_ID,
  name: 'Create due date notification',
  icon: 'bolt',
  status: 'active',
  trigger: 'operation',
  accountability: 'all',
  options: { return: '$last' },
  operation: CREATE_DUE_DATE_NOTIFICATION_OPERATION.id
}

module.exports = {
  async up(knex) {
    await knex('directus_flows').insert([
      DUE_SYNTHESES_D_ENTRETIEN_NOTIFICATIONS_FLOW,
      DUE_DEMANDES_D_AIDES_NOTIFICATIONS_FLOW,
      CREATE_DUE_DATE_NOTIFICATION_FLOW
    ])

    await knex('directus_operations').insert([
      DUE_SYNTHESES_D_ENTRETIEN_TRIGGER_FLOW_OPERATION,
      DUE_TODAY_SYNTHESES_D_ENTRETIEN_OPERATION,
      DUE_DEMANDES_D_AIDES_TRIGGER_FLOW_OPERATION,
      DUE_TODAY_DEMANDES_D_AIDES_OPERATION,
      CREATE_DUE_DATE_NOTIFICATION_OPERATION
    ])
  },

  down() {}
}
