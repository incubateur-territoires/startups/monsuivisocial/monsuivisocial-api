const {
  AGENT_D_ACCUEIL_ID,
  REFERENT_ID,
  BENEFICIARY_REFERENT,
  INSTRUCTEUR_ID
} = require('../../utils/constants')

module.exports = {
  async up(knex) {
    // When updating an user to a stricter role, it can leads to access errors

    await knex('directus_permissions')
      .whereIn('action', ['read', 'update'])
      .andWhere({ role: AGENT_D_ACCUEIL_ID, collection: 'notifications' })
      .update({
        permissions: {
          _and: [
            { recipient: { id: { _eq: '$CURRENT_USER' } } },

            /**
             * A new document notification can not be accessed by an Agent
             * d'accueil as Agent d'accueil can only access documents they have
             * uploaded
             */
            { type: { _neq: 'new_document' } }
          ]
        }
      })

    await knex('directus_permissions')
      .whereIn('action', ['read', 'update'])
      .andWhere({ role: REFERENT_ID, collection: 'notifications' })
      .update({
        permissions: {
          _and: [
            { recipient: { id: { _eq: '$CURRENT_USER' } } },

            /**
             * Référents can not access items related to beneficiaries they are
             * not referents. So they can not access notifications related to
             * such items.
             */
            {
              _or: [
                {
                  item: {
                    'item:help_requests': { beneficiary: BENEFICIARY_REFERENT }
                  }
                },
                {
                  item: {
                    'item:follow_ups': { beneficiary: BENEFICIARY_REFERENT }
                  }
                },
                {
                  item: {
                    'item:directus_files': { beneficiary: BENEFICIARY_REFERENT }
                  }
                }
              ]
            }
          ]
        }
      })

    await knex('directus_permissions')
      .whereIn('action', ['read', 'update'])
      .andWhere({ role: INSTRUCTEUR_ID, collection: 'notifications' })
      .update({
        permissions: {
          _and: [
            { recipient: { id: { _eq: '$CURRENT_USER' } } },

            /**
             * Instructeurs can not access to confidential documents they have
             * not uploaded so they can not access notifications related to such
             * documents.
             */
            {
              _or: [
                { item: { collection: { _neq: 'directus_files' } } },
                {
                  item: {
                    'item:directus_files': { confidential: { _eq: false } }
                  }
                },
                {
                  item: {
                    'item:directus_files': {
                      uploaded_by: { id: { _eq: '$CURRENT_USER' } }
                    }
                  }
                }
              ]
            }
          ]
        }
      })
  },

  down() {}
}
