const { SEND_EMAIL_OPERATION_ID } = require('../../utils/constants')

module.exports = {
  async up(knex) {
    await knex('directus_operations')
      .where({ id: SEND_EMAIL_OPERATION_ID })
      .update({
        options: {
          to: '{{$trigger.payload.email}}',
          subject: 'Bienvenue sur Mon Suivi Social',
          body: "Bonjour,\n\nVotre responsable de structure vous a ouvert des accès à l'application Mon Suivi Social.\n\nPour s'authentifier, Mon Suivi Social utilise l'outil Inclusion Connect.\n\nAfin d'accéder à l'application, veuillez créer un compte InclusionConnect lors de votre premier accès.\n\n[Créer son compte Inclusion Connect](http://localhost:3000/registration)\n\nEn cas de difficulté, contactez-nous à monsuivisocial@anct.gouv.fr.\n\nL'équipe de Mon Suivi Social\n\n" // FIXME: Find a way to have redirect URL adapted to environment
        }
      })
  },

  async down(knex) {
    await knex('directus_operations')
      .where({ id: SEND_EMAIL_OPERATION_ID })
      .update({
        options: {
          to: '{{$trigger.payload.email}}',
          subject: 'Bienvenue sur Mon Suivi Social',
          body: "Bonjour,\n\nVotre responsable de structure vous a créé un compte sur l'application Mon Suivi Social.\n\nPour s'authentifier, Mon Suivi Social utilise l'outil Inclusion Connect qui vous permettra d'accéder à plusieurs outils numériques de l'inclusion, en plus de Mon Suivi Social.\n\nAfin d'accéder à l'application, veuillez cliquer sur le lien ci-dessous pour créer votre compte Inclusion Connect.\n\n[Créer son compte Inclusion Connect](https://connect.inclusion.beta.gouv.fr/realms/inclusion-connect/login-actions/registration?client_id=monsuivisocial)\n\nL'équipe de Mon Suivi Social\n\n"
        }
      })
  }
}
