const { insertPermission } = require('../../utils')
const {
  RESPONSABLE_STRUCTURE_ID,
  TRAVAILLEUR_SOCIAL_ID,
  INSTRUCTEUR_ID,
  REFERENT_ID,
  BENEFICIARY_AGENT_IN_SAME_ORGANISATION,
  BENEFICIARY_REFERENT
} = require('../../utils/constants')

module.exports = {
  async up(knex) {
    await insertResponsableDeStructurePermissions(knex)
    await insertTravailleurSocialPermissions(knex)
    await insertInstructeurPermissions(knex)
    await insertReferentPermissions(knex)
  },

  async down(knex) {
    await knex('directus_permissions')
      .where({
        collection: 'help_requests_directus_files'
      })
      .del()
  }
}

async function insertResponsableDeStructurePermissions(knex) {
  await insertPermission(
    knex,
    RESPONSABLE_STRUCTURE_ID,
    'help_requests_directus_files',
    'create',
    {},
    '*'
  )

  await insertPermission(
    knex,
    RESPONSABLE_STRUCTURE_ID,
    'help_requests_directus_files',
    'read',
    {
      _and: [
        {
          _or: [
            {
              help_request: {
                beneficiary: BENEFICIARY_AGENT_IN_SAME_ORGANISATION
              }
            },
            {
              directus_file: {
                beneficiary: BENEFICIARY_AGENT_IN_SAME_ORGANISATION
              }
            }
          ]
        }
      ]
    },
    '*'
  )

  await insertPermission(
    knex,
    RESPONSABLE_STRUCTURE_ID,
    'help_requests_directus_files',
    'delete',
    {
      _and: [
        {
          _or: [
            {
              help_request: {
                beneficiary: BENEFICIARY_AGENT_IN_SAME_ORGANISATION
              }
            },
            {
              directus_file: {
                beneficiary: BENEFICIARY_AGENT_IN_SAME_ORGANISATION
              }
            }
          ]
        }
      ]
    }
  )
}

async function insertTravailleurSocialPermissions(knex) {
  await insertPermission(
    knex,
    TRAVAILLEUR_SOCIAL_ID,
    'help_requests_directus_files',
    'create',
    {},
    '*'
  )

  await insertPermission(
    knex,
    TRAVAILLEUR_SOCIAL_ID,
    'help_requests_directus_files',
    'read',
    {
      _and: [
        {
          _or: [
            {
              help_request: {
                beneficiary: BENEFICIARY_AGENT_IN_SAME_ORGANISATION
              }
            },
            {
              directus_file: {
                beneficiary: BENEFICIARY_AGENT_IN_SAME_ORGANISATION
              }
            }
          ]
        }
      ]
    },
    '*'
  )

  await insertPermission(
    knex,
    TRAVAILLEUR_SOCIAL_ID,
    'help_requests_directus_files',
    'delete',
    {
      _and: [
        {
          _or: [
            {
              help_request: {
                beneficiary: BENEFICIARY_AGENT_IN_SAME_ORGANISATION
              }
            },
            {
              directus_file: {
                beneficiary: BENEFICIARY_AGENT_IN_SAME_ORGANISATION
              }
            }
          ]
        }
      ]
    }
  )
}

async function insertInstructeurPermissions(knex) {
  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'help_requests_directus_files',
    'create',
    {},
    '*'
  )

  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'help_requests_directus_files',
    'read',
    {
      _and: [
        {
          _or: [
            {
              help_request: {
                beneficiary: BENEFICIARY_AGENT_IN_SAME_ORGANISATION
              }
            },
            {
              directus_file: {
                beneficiary: BENEFICIARY_AGENT_IN_SAME_ORGANISATION
              }
            }
          ]
        }
      ]
    },
    '*'
  )

  await insertPermission(
    knex,
    INSTRUCTEUR_ID,
    'help_requests_directus_files',
    'delete',
    {
      _and: [
        {
          _or: [
            {
              help_request: {
                beneficiary: BENEFICIARY_AGENT_IN_SAME_ORGANISATION
              }
            },
            {
              directus_file: {
                beneficiary: BENEFICIARY_AGENT_IN_SAME_ORGANISATION
              }
            }
          ]
        }
      ]
    }
  )
}

async function insertReferentPermissions(knex) {
  await insertPermission(
    knex,
    REFERENT_ID,
    'help_requests_directus_files',
    'create',
    {},
    '*'
  )

  await insertPermission(
    knex,
    REFERENT_ID,
    'help_requests_directus_files',
    'read',
    {
      _and: [
        {
          _or: [
            { help_request: { beneficiary: BENEFICIARY_REFERENT } },
            { directus_file: { beneficiary: BENEFICIARY_REFERENT } }
          ]
        }
      ]
    },
    '*'
  )

  await insertPermission(
    knex,
    REFERENT_ID,
    'help_requests_directus_files',
    'delete',
    {
      _and: [
        {
          _or: [
            { help_request: { beneficiary: BENEFICIARY_REFERENT } },
            { directus_file: { beneficiary: BENEFICIARY_REFERENT } }
          ]
        }
      ]
    }
  )
}
