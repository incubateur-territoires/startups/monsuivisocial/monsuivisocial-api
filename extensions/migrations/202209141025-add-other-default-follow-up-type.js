const { randomUUID } = require('crypto')

const OTHER = {
  id: randomUUID(),
  name: 'Autre',
  type: 'optional',
  default: true
}

module.exports = {
  async up(knex) {
    await knex('follow_up_types').insert(OTHER)
  },

  async down() {
    // Downgrading this migration may not be really useful. If it is, knowing the id of the Other follow up type that has been previously created may be an issue.
  }
}
