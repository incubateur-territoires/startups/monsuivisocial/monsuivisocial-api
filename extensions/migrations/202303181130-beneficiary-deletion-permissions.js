const {
  TRAVAILLEUR_SOCIAL_ID,
  BENEFICIARY_AGENT_IN_SAME_ORGANISATION
} = require('../../utils/constants')

module.exports = {
  async up(knex) {
    await knex('directus_permissions')
      .where({
        role: TRAVAILLEUR_SOCIAL_ID,
        collection: 'help_requests',
        action: 'delete'
      })
      .update({
        permissions: {
          _and: [BENEFICIARY_AGENT_IN_SAME_ORGANISATION]
        }
      })
  },

  async down() {}
}
