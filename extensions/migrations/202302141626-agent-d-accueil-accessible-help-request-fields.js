const { AGENT_D_ACCUEIL_ID } = require('../../utils/constants')

module.exports = {
  async up(knex) {
    await knex('directus_permissions')
      .where({
        role: AGENT_D_ACCUEIL_ID,
        collection: 'help_requests',
        action: 'read'
      })
      .update({
        fields:
          'follow_up_types,follow_up_type,beneficiary,id,status,user_created'
      })
  },

  down() {}
}
