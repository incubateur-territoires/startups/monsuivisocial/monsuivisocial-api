const { RESPONSABLE_STRUCTURE_ID } = require('../../utils/constants')

module.exports = {
  async up(knex) {
    await knex('directus_permissions')
      .andWhere({
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'directus_users',
        action: 'read'
      })
      .update({
        fields:
          'organisation,aidant_connect_authorisation,first_name,last_name,email,role,status,id'
      })
  },

  async down() {}
}
