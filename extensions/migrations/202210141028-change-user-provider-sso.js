const {
  RESPONSABLE_STRUCTURE_ID,
  TRAVAILLEUR_SOCIAL_ID
} = require('../../utils/constants')

module.exports = {
  async up(knex) {
    // FIXME: This migration command should be removed once deployed on all environments
    await knex('directus_users')
      .whereIn('role', [RESPONSABLE_STRUCTURE_ID, TRAVAILLEUR_SOCIAL_ID])
      .update({
        provider: 'keycloak',
        external_identifier: knex.ref('email')
      })

    await knex('directus_permissions')
      .where({
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'directus_users',
        action: 'create'
      })
      .update({
        fields:
          'id,first_name,last_name,email,role,language,theme,organisation,provider,external_identifier'
      })
  },

  async down(knex) {
    // FIXME: This migration command should be removed once deployed on all environments
    await knex('directus_users').update({
      provider: 'default',
      external_identifier: null
    })

    await knex('directus_permissions')
      .where({
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'directus_users',
        action: 'create'
      })
      .update({
        fields: 'id,first_name,last_name,email,role,language,theme,organisation'
      })
  }
}
