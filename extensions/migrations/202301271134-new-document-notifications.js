const { INSTRUCTEUR_ID, AGENT_D_ACCUEIL_ID } = require('../../utils/constants')

const NEW_DOCUMENT_NOTIFICATIONS_FLOW_ID =
  '02d2c38d-a4e4-4d53-8105-8b8fb6f401ad'
const CREATE_NEW_DOCUMENT_NOTIFICATION_FLOW_ID =
  'a0a7c125-176f-40e1-92b1-3b636f6e59e1'

const TRIGGER_CREATE_NEW_DOCUMENT_NOTIFICATION_1_OPERATION = {
  id: 'd33238b9-9708-4d5f-bfb9-75aa0a8adf1d',
  name: 'Trigger Flow',
  key: 'trigger_create_new_document_notification_1',
  type: 'trigger',
  position_x: 73,
  position_y: 1,
  options: {
    flow: CREATE_NEW_DOCUMENT_NOTIFICATION_FLOW_ID,
    payload: '{{ $last }}'
  },
  flow: NEW_DOCUMENT_NOTIFICATIONS_FLOW_ID
}

const PREPARE_NOTIFICATION_DATA_1_OPERATION = {
  id: '340be7ba-3cd6-46d8-a7c0-ec2d1f68a6fe',
  name: 'Prepare notification data',
  key: 'prepare_notification_data_1',
  type: 'exec',
  position_x: 55,
  position_y: 1,
  options: {
    code: 'module.exports = async function(data) {\n\treturn data.$last.map(r => ({recipient: r.referent.id, item: data.$trigger.key}));\n}'
  },
  resolve: TRIGGER_CREATE_NEW_DOCUMENT_NOTIFICATION_1_OPERATION.id,
  flow: NEW_DOCUMENT_NOTIFICATIONS_FLOW_ID
}

const READ_REFERENTS_WITHOUT_INSTRUCTEUR_OPERATION = {
  id: '1c5c09a0-8d99-49a8-b6bd-73f87bc514c2',
  name: 'Read referents without instructeur',
  key: 'read_referents_without_instructeur',
  type: 'item-read',
  position_x: 37,
  position_y: 1,
  options: {
    collection: 'beneficiary_referents',
    query: {
      filter: {
        _and: [
          { beneficiary: '{{ $trigger.payload.beneficiary }}' },
          {
            referent: {
              role: {
                _nin: [INSTRUCTEUR_ID, AGENT_D_ACCUEIL_ID]
              }
            }
          },
          { referent: { _neq: '{{ $accountability.user }}' } }
        ]
      },
      fields: ['referent.id']
    },
    key: null,
    permissions: '$full'
  },
  resolve: PREPARE_NOTIFICATION_DATA_1_OPERATION.id,
  flow: NEW_DOCUMENT_NOTIFICATIONS_FLOW_ID
}

const TRIGGER_CREATE_NEW_DOCUMENT_NOTIFICATION_2_OPERATION = {
  id: 'ab7dfee9-6996-40f0-bf4d-0b47569a067e',
  name: 'Trigger Flow',
  key: 'trigger_create_new_document_notification_2',
  type: 'trigger',
  position_x: 73,
  position_y: 16,
  options: {
    flow: CREATE_NEW_DOCUMENT_NOTIFICATION_FLOW_ID,
    payload: '{{ $last }}'
  },
  flow: NEW_DOCUMENT_NOTIFICATIONS_FLOW_ID
}

const PREPARE_NOTIFICATION_DATA_2_OPERATION = {
  id: 'ba59f276-8481-4e43-b467-032fb02f86c3',
  name: 'Prepare notification data',
  key: 'prepare_notification_data_2',
  type: 'exec',
  position_x: 55,
  position_y: 16,
  options: {
    code: 'module.exports = async function(data) {\n\treturn data.$last.map(r => ({recipient: r.referent.id, item: data.$trigger.key}));\n}'
  },
  resolve: TRIGGER_CREATE_NEW_DOCUMENT_NOTIFICATION_2_OPERATION.id,
  flow: NEW_DOCUMENT_NOTIFICATIONS_FLOW_ID
}

const READ_REFERENTS_OPERATION = {
  id: 'e8625d7e-35a1-491f-8a1c-54b8448dd65a',
  name: 'Read referents',
  key: 'read_referents',
  type: 'item-read',
  position_x: 37,
  position_y: 16,
  options: {
    collection: 'beneficiary_referents',
    query: {
      filter: {
        _and: [
          { beneficiary: '{{ $trigger.payload.beneficiary }}' },
          {
            referent: { role: { _neq: AGENT_D_ACCUEIL_ID } }
          },
          { referent: { _neq: '{{ $accountability.user }}' } }
        ]
      },
      fields: ['referent.id']
    },
    key: null,
    permissions: '$full'
  },
  resolve: PREPARE_NOTIFICATION_DATA_2_OPERATION.id,
  flow: NEW_DOCUMENT_NOTIFICATIONS_FLOW_ID
}

const IS_CONFIDENTIAL_DOCUMENT_OPERATION = {
  id: '416af183-4992-4c39-879d-1603e30dc46a',
  name: 'Is confidential document',
  key: 'is_confidential_document',
  type: 'condition',
  position_x: 19,
  position_y: 1,
  options: {
    filter: { $trigger: { payload: { confidential: { _eq: true } } } }
  },
  resolve: READ_REFERENTS_WITHOUT_INSTRUCTEUR_OPERATION.id,
  reject: READ_REFERENTS_OPERATION.id,
  flow: NEW_DOCUMENT_NOTIFICATIONS_FLOW_ID
}

const NEW_DOCUMENT_NOTIFICATIONS_FLOW = {
  id: NEW_DOCUMENT_NOTIFICATIONS_FLOW_ID,
  name: 'New document notifications',
  icon: 'bolt',
  status: 'active',
  trigger: 'event',
  accountability: 'all',
  options: {
    type: 'action',
    scope: ['files.upload'],
    collections: ['directus_files']
  },
  operation: IS_CONFIDENTIAL_DOCUMENT_OPERATION.id
}

const CREATE_NEW_DOCUMENT_NOTIFICATION_OPERATION = {
  id: '62a72f99-02da-43b8-9ed0-0a546c7600a0',
  name: 'Create new document notification',
  key: 'create_new_document_notification',
  type: 'item-create',
  position_x: 19,
  position_y: 1,
  options: {
    collection: 'notifications',
    payload: {
      type: 'new_document',
      recipient: '{{ $trigger.recipient }}',
      item: [{ item: '{{ $trigger.item }}', collection: 'directus_files' }]
    },
    permissions: '$full'
  },
  flow: CREATE_NEW_DOCUMENT_NOTIFICATION_FLOW_ID
}

const CREATE_NEW_DOCUMENT_NOTIFICATION_FLOW = {
  id: CREATE_NEW_DOCUMENT_NOTIFICATION_FLOW_ID,
  name: 'Create new document notification',
  icon: 'bolt',
  status: 'active',
  trigger: 'operation',
  accountability: 'all',
  options: { return: '$all' },
  operation: CREATE_NEW_DOCUMENT_NOTIFICATION_OPERATION.id
}

module.exports = {
  async up(knex) {
    await knex('directus_flows').insert([
      NEW_DOCUMENT_NOTIFICATIONS_FLOW,
      CREATE_NEW_DOCUMENT_NOTIFICATION_FLOW
    ])

    await knex('directus_operations').insert([
      TRIGGER_CREATE_NEW_DOCUMENT_NOTIFICATION_1_OPERATION,
      PREPARE_NOTIFICATION_DATA_1_OPERATION,
      READ_REFERENTS_WITHOUT_INSTRUCTEUR_OPERATION,
      TRIGGER_CREATE_NEW_DOCUMENT_NOTIFICATION_2_OPERATION,
      PREPARE_NOTIFICATION_DATA_2_OPERATION,
      READ_REFERENTS_OPERATION,
      IS_CONFIDENTIAL_DOCUMENT_OPERATION,
      CREATE_NEW_DOCUMENT_NOTIFICATION_OPERATION
    ])
  },

  down() {}
}
