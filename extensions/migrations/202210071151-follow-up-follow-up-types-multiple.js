const { setSequenceValue } = require('../../utils')
const {
  RESPONSABLE_STRUCTURE_ID,
  DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION,
  TRAVAILLEUR_SOCIAL_ID
} = require('../../utils/constants')

module.exports = {
  async up(knex) {
    await addPermissionsToFollowUpsFollowUpTypesM2M(knex)
  },

  async down(knex) {
    await knex('directus_permissions')
      .where('collection', 'follow_ups_follow_up_types')
      .del()
    await setSequenceValue(knex, 'public.directus_permissions_id_seq', 76)
  }
}

async function addPermissionsToFollowUpsFollowUpTypesM2M(knex) {
  await knex('directus_permissions').insert([
    {
      role: RESPONSABLE_STRUCTURE_ID,
      collection: 'follow_ups_follow_up_types',
      action: 'create',
      permissions: {},
      fields: '*'
    },
    {
      role: RESPONSABLE_STRUCTURE_ID,
      collection: 'follow_ups_follow_up_types',
      action: 'read',
      permissions: {
        _and: [
          {
            follow_ups_id: {
              beneficiary: DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION
            }
          }
        ]
      },
      fields: '*'
    },
    {
      role: RESPONSABLE_STRUCTURE_ID,
      collection: 'follow_ups_follow_up_types',
      action: 'delete',
      permissions: {
        _and: [
          {
            follow_ups_id: {
              beneficiary: DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION
            }
          }
        ]
      },
      fields: '*'
    },
    {
      role: TRAVAILLEUR_SOCIAL_ID,
      collection: 'follow_ups_follow_up_types',
      action: 'create',
      permissions: {},
      fields: '*'
    },
    {
      role: TRAVAILLEUR_SOCIAL_ID,
      collection: 'follow_ups_follow_up_types',
      action: 'read',
      permissions: {
        _and: [
          {
            follow_ups_id: {
              beneficiary: DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION
            }
          }
        ]
      },
      fields: '*'
    },
    {
      role: TRAVAILLEUR_SOCIAL_ID,
      collection: 'follow_ups_follow_up_types',
      action: 'delete',
      permissions: {
        _and: [
          {
            follow_ups_id: {
              beneficiary: DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION
            }
          }
        ]
      },
      fields: '*'
    }
  ])

  await setSequenceValue(knex, 'public.directus_permissions_id_seq', 82)
}
