const { insertPermission } = require('../../utils/index')
const {
  TRAVAILLEUR_SOCIAL_ID,
  REFERENT_ID,
  INSTRUCTEUR_ID
} = require('../../utils/constants')
const { CREATED_BY_CURRENT_USER } = require('../../utils/constants')

module.exports = {
  async up(knex) {
    await insertPermission(
      knex,
      TRAVAILLEUR_SOCIAL_ID,
      'follow_ups',
      'delete',
      { _and: [CREATED_BY_CURRENT_USER] }
    )
    await insertPermission(
      knex,
      TRAVAILLEUR_SOCIAL_ID,
      'help_requests',
      'delete',
      { _and: [CREATED_BY_CURRENT_USER] }
    )

    await insertPermission(knex, INSTRUCTEUR_ID, 'follow_ups', 'delete', {
      _and: [CREATED_BY_CURRENT_USER]
    })
    await insertPermission(knex, INSTRUCTEUR_ID, 'help_requests', 'delete', {
      _and: [CREATED_BY_CURRENT_USER]
    })

    await insertPermission(knex, REFERENT_ID, 'follow_ups', 'delete', {
      _and: [CREATED_BY_CURRENT_USER]
    })
    await insertPermission(knex, REFERENT_ID, 'help_requests', 'delete', {
      _and: [CREATED_BY_CURRENT_USER]
    })
  },

  async down() {}
}
