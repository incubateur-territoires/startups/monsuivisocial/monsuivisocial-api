const { RESPONSABLE_STRUCTURE_ID } = require('../../utils/constants')

module.exports = {
  async up(knex) {
    await knex('directus_permissions')
      .where({
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'organisation',
        action: 'update'
      })
      .update({
        fields:
          'id,user_created,date_created,user_updated,date_updated,name,zip_code,city,address,phone,email,follow_up_types'
      })
  },

  async down(knex) {}
}
