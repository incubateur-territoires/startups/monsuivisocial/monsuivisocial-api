const {
  AGENT_D_ACCUEIL_ID,
  RESPONSABLE_STRUCTURE_ID,
  TRAVAILLEUR_SOCIAL_ID,
  INSTRUCTEUR_ID,
  REFERENT_ID
} = require('../../utils/constants')
const { insertPermission } = require('../../utils')

module.exports = {
  async up(knex) {
    await insertNotificationsPermissions(knex, RESPONSABLE_STRUCTURE_ID)
    await insertNotificationsPermissions(knex, TRAVAILLEUR_SOCIAL_ID)
    await insertNotificationsPermissions(knex, INSTRUCTEUR_ID)
    await insertNotificationsPermissions(knex, REFERENT_ID)
    await insertNotificationsPermissions(knex, AGENT_D_ACCUEIL_ID)
  },

  async down() {}
}

async function insertNotificationsPermissions(knex, role) {
  await insertPermission(
    knex,
    role,
    'notifications',
    'read',
    { _and: [{ recipient: { id: { _eq: '$CURRENT_USER' } } }] },
    '*'
  )

  await insertPermission(
    knex,
    role,
    'notifications',
    'update',
    { _and: [{ recipient: { id: { _eq: '$CURRENT_USER' } } }] },
    'read'
  )

  await insertPermission(
    knex,
    role,
    'notification_items',
    'read',
    {
      _and: [{ notification: { recipient: { id: { _eq: '$CURRENT_USER' } } } }]
    },
    '*'
  )
}
