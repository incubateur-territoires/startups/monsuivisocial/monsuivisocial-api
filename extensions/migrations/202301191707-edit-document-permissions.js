const {
  AGENT_D_ACCUEIL_ID,
  RESPONSABLE_STRUCTURE_ID,
  TRAVAILLEUR_SOCIAL_ID,
  INSTRUCTEUR_ID,
  REFERENT_ID
} = require('../../utils/constants')
const { insertPermission } = require('../../utils')

module.exports = {
  async up(knex) {
    await insertPermission(
      knex,
      AGENT_D_ACCUEIL_ID,
      'directus_files',
      'update',
      { _and: [{ uploaded_by: { id: { _eq: '$CURRENT_USER' } } }] },
      'filename_download,document_type,confidential,labels'
    )

    await knex('directus_permissions')
      .whereIn('role', [
        RESPONSABLE_STRUCTURE_ID,
        TRAVAILLEUR_SOCIAL_ID,
        INSTRUCTEUR_ID,
        REFERENT_ID
      ])
      .andWhere({
        collection: 'directus_files',
        action: 'update'
      })
      .update({
        fields: 'filename_download,document_type,confidential,labels'
      })
  },

  async down() {}
}
