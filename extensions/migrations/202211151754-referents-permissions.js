const {
  RESPONSABLE_STRUCTURE_ID,
  TRAVAILLEUR_SOCIAL_ID,
  DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION,
  BENEFICIARY_AGENT_IN_SAME_ORGANISATION,
  DEPRECATED_RELATIVE_IN_SAME_ORGANISATION,
  RELATIVE_IN_SAME_ORGANISATION,
  USER_IN_SAME_ORGANISATION
} = require('../../utils/constants')
const { setSequenceValue, insertPermission } = require('../../utils')

module.exports = {
  async up(knex) {
    await knex('directus_permissions')
      .whereIn('role', [RESPONSABLE_STRUCTURE_ID, TRAVAILLEUR_SOCIAL_ID])
      .andWhere({ collection: 'beneficiary' })
      .andWhereNot({ action: 'create' })
      .update({
        permissions: {
          _and: [BENEFICIARY_AGENT_IN_SAME_ORGANISATION]
        }
      })

    await knex('directus_permissions')
      .whereIn('role', [RESPONSABLE_STRUCTURE_ID, TRAVAILLEUR_SOCIAL_ID])
      .andWhere({ collection: 'relatives' })
      .andWhereNot({ action: 'create' })
      .update({
        permissions: { _and: [RELATIVE_IN_SAME_ORGANISATION] }
      })

    await knex('directus_permissions')
      .whereIn(
        ['role', 'collection', 'action'],
        [
          [RESPONSABLE_STRUCTURE_ID, 'follow_ups', 'read'],
          [RESPONSABLE_STRUCTURE_ID, 'follow_ups', 'update'],
          [RESPONSABLE_STRUCTURE_ID, 'help_requests', 'read'],
          [RESPONSABLE_STRUCTURE_ID, 'help_requests', 'update'],
          [TRAVAILLEUR_SOCIAL_ID, 'follow_ups', 'read'],
          [TRAVAILLEUR_SOCIAL_ID, 'follow_ups', 'update'],
          [TRAVAILLEUR_SOCIAL_ID, 'help_requests', 'read'],
          [TRAVAILLEUR_SOCIAL_ID, 'help_requests', 'update']
        ]
      )
      .update({
        permissions: {
          _and: [{ beneficiary: BENEFICIARY_AGENT_IN_SAME_ORGANISATION }]
        }
      })

    await knex('directus_permissions')
      .whereIn('action', ['read', 'delete'])
      .andWhere({
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'follow_ups_follow_up_types'
      })
      .update({
        permissions: {
          _and: [
            {
              follow_ups_id: {
                beneficiary: BENEFICIARY_AGENT_IN_SAME_ORGANISATION
              }
            }
          ]
        }
      })

    const beneficiaryReferentsPermissions = [
      {
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'beneficiary_referents',
        action: 'create',
        permissions: {},
        fields: '*'
      },
      {
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'beneficiary_referents',
        action: 'read',
        permissions: { _and: [{ referent: USER_IN_SAME_ORGANISATION }] },
        fields: '*'
      },
      {
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'beneficiary_referents',
        action: 'delete',
        permissions: { _and: [{ referent: USER_IN_SAME_ORGANISATION }] }
      },
      {
        role: TRAVAILLEUR_SOCIAL_ID,
        collection: 'beneficiary_referents',
        action: 'create',
        permissions: {},
        fields: '*'
      },
      {
        role: TRAVAILLEUR_SOCIAL_ID,
        collection: 'beneficiary_referents',
        action: 'read',
        permissions: { _and: [{ referent: USER_IN_SAME_ORGANISATION }] },
        fields: '*'
      },
      {
        role: TRAVAILLEUR_SOCIAL_ID,
        collection: 'beneficiary_referents',
        action: 'delete',
        permissions: { _and: [{ referent: USER_IN_SAME_ORGANISATION }] }
      }
    ]

    for (const {
      role,
      collection,
      action,
      permissions,
      fields
    } of beneficiaryReferentsPermissions) {
      await insertPermission(
        knex,
        role,
        collection,
        action,
        permissions,
        fields
      )
    }
  },

  async down(knex) {
    await knex('directus_permissions')
      .whereIn('role', [RESPONSABLE_STRUCTURE_ID, TRAVAILLEUR_SOCIAL_ID])
      .andWhere({ collection: 'beneficiary' })
      .andWhereNot({ action: 'create' })
      .update({
        permissions: {
          _and: [DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION]
        }
      })

    await knex('directus_permissions')
      .whereIn('role', [RESPONSABLE_STRUCTURE_ID, TRAVAILLEUR_SOCIAL_ID])
      .andWhere({ collection: 'relatives' })
      .andWhereNot({ action: 'create' })
      .update({
        permissions: { _and: [DEPRECATED_RELATIVE_IN_SAME_ORGANISATION] }
      })

    await knex('directus_permissions')
      .whereIn(
        ['role', 'collection', 'action'],
        [
          [RESPONSABLE_STRUCTURE_ID, 'follow_ups', 'read'],
          [RESPONSABLE_STRUCTURE_ID, 'follow_ups', 'update'],
          [RESPONSABLE_STRUCTURE_ID, 'help_requests', 'read'],
          [RESPONSABLE_STRUCTURE_ID, 'help_requests', 'update'],
          [TRAVAILLEUR_SOCIAL_ID, 'follow_ups', 'read'],
          [TRAVAILLEUR_SOCIAL_ID, 'follow_ups', 'update'],
          [TRAVAILLEUR_SOCIAL_ID, 'help_requests', 'read'],
          [TRAVAILLEUR_SOCIAL_ID, 'help_requests', 'update']
        ]
      )
      .update({
        permissions: {
          _and: [
            { beneficiary: DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION }
          ]
        }
      })

    await knex('directus_permissions')
      .whereIn('action', ['read', 'delete'])
      .andWhere({
        role: RESPONSABLE_STRUCTURE_ID,
        collection: 'follow_ups_follow_up_types'
      })
      .update({
        permissions: {
          _and: [
            {
              follow_ups_id: {
                beneficiary: DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION
              }
            }
          ]
        }
      })

    await knex('directus_permissions')
      .where({
        collection: 'beneficiary_referents'
      })
      .del()

    await setSequenceValue(knex, 'public.directus_permissions_id_seq', 93)
  }
}
