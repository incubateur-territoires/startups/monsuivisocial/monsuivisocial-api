const { insertPermission } = require('../../utils')
const {
  SAME_ORGANISATION,
  CREATED_BY_CURRENT_USER,
  USER_IN_SAME_ORGANISATION,
  BENEFICIARY_AGENT_IN_SAME_ORGANISATION,
  AGENT_D_ACCUEIL_ID
} = require('../../utils/constants')

module.exports = {
  async up(knex) {
    await createAgentDAccueilRoleAndPermissions(knex)
  },

  async down(knex) {
    await deleteAgentDAccueilRoleAndPermissions(knex)
  }
}

async function createAgentDAccueilRoleAndPermissions(knex) {
  await knex('directus_roles').insert({
    id: AGENT_D_ACCUEIL_ID,
    name: "Agent d'accueil/CNFS",
    description:
      "Peut gérer tous les bénéficiaires de sa structure avec des droits limités. Ne peut pas instruire de demande d'aide.",
    enforce_tfa: false,
    admin_access: false,
    app_access: false
  })

  await insertPermission(
    knex,
    AGENT_D_ACCUEIL_ID,
    'directus_roles',
    'read',
    {},
    '*'
  )

  await insertPermission(
    knex,
    AGENT_D_ACCUEIL_ID,
    'directus_users',
    'read',
    { _and: [USER_IN_SAME_ORGANISATION] },
    'id,first_name,last_name,email,role,organisation'
  )

  await insertPermission(
    knex,
    AGENT_D_ACCUEIL_ID,
    'directus_revisions',
    'read',
    {},
    '*'
  )

  await insertPermission(
    knex,
    AGENT_D_ACCUEIL_ID,
    'directus_activity',
    'read',
    {},
    '*'
  )

  await createAgentDAccueilBeneficiaryPermissions(knex)

  await insertPermission(
    knex,
    AGENT_D_ACCUEIL_ID,
    'beneficiary_referents',
    'create',
    {},
    '*'
  )
  await insertPermission(
    knex,
    AGENT_D_ACCUEIL_ID,
    'beneficiary_referents',
    'read',
    { _and: [{ referent: USER_IN_SAME_ORGANISATION }] },
    '*'
  )

  await createAgentDAccueilDirectusFilesPermissions(knex)
  await createAgentDAccueilFollowUpsPermissions(knex)
  await createAgentDAccueilFollowUpsFollowUpTypesPermissions(knex)
  await createAgentDAccueilHelpRequestsPermissions(knex)

  await insertPermission(
    knex,
    AGENT_D_ACCUEIL_ID,
    'follow_up_types',
    'read',
    {
      _and: [{ organisations: { organisation_id: SAME_ORGANISATION } }]
    },
    '*'
  )

  await insertPermission(
    knex,
    AGENT_D_ACCUEIL_ID,
    'directus_folders',
    'read',
    {
      _and: [{ name: { _contains: 'Beneficiary documents' } }]
    },
    '*'
  )

  await insertPermission(
    knex,
    AGENT_D_ACCUEIL_ID,
    'organisation',
    'read',
    { _and: [SAME_ORGANISATION] },
    '*'
  )

  await insertPermission(
    knex,
    AGENT_D_ACCUEIL_ID,
    'organisation_follow_up_types',
    'read',
    { _and: [{ organisation_id: SAME_ORGANISATION }] },
    '*'
  )
}

async function createAgentDAccueilBeneficiaryPermissions(knex) {
  await insertPermission(
    knex,
    AGENT_D_ACCUEIL_ID,
    'beneficiary',
    'create',
    {},
    '*'
  )
  await insertPermission(
    knex,
    AGENT_D_ACCUEIL_ID,
    'beneficiary',
    'read',
    { _and: [BENEFICIARY_AGENT_IN_SAME_ORGANISATION] },
    'id,user_created,date_created,user_updated,date_updated,aidant_connect_authorisation,accordion-x8aezq,status,title,usual_name,birth_name,firstname,birthdate,birth_place,deathdate,gender,nationality,accommodation_mode,accommodation_name,accommodation_additional_information,street,street_number,address_complement,zip_code,city,no_phone,phone_1,phone_2,email,family_situation,caregiver,minor_children,major_children,mobility,administration,minister,follow_ups,referents,help_requests,file_number'
  )
  await insertPermission(
    knex,
    AGENT_D_ACCUEIL_ID,
    'beneficiary',
    'update',
    { _and: [BENEFICIARY_AGENT_IN_SAME_ORGANISATION] },
    'id,user_created,date_created,user_updated,date_updated,aidant_connect_authorisation,accordion-x8aezq,status,title,usual_name,birth_name,firstname,birthdate,birth_place,deathdate,gender,nationality,accommodation_mode,accommodation_name,accommodation_additional_information,street,street_number,address_complement,zip_code,city,no_phone,phone_1,phone_2,email,family_situation,caregiver,minor_children,major_children,mobility,administration,minister,follow_ups'
  )
}

async function createAgentDAccueilDirectusFilesPermissions(knex) {
  await insertPermission(
    knex,
    AGENT_D_ACCUEIL_ID,
    'directus_files',
    'create',
    {},
    '*'
  )
  await insertPermission(
    knex,
    AGENT_D_ACCUEIL_ID,
    'directus_files',
    'read',
    { _and: [{ beneficiary: BENEFICIARY_AGENT_IN_SAME_ORGANISATION }] },
    '*'
  )
  await insertPermission(
    knex,
    AGENT_D_ACCUEIL_ID,
    'directus_files',
    'update',
    { _and: [{ beneficiary: BENEFICIARY_AGENT_IN_SAME_ORGANISATION }] },
    '*'
  )
}

async function createAgentDAccueilFollowUpsPermissions(knex) {
  await insertPermission(
    knex,
    AGENT_D_ACCUEIL_ID,
    'follow_ups',
    'create',
    {},
    'id,user_created,date_created,user_updated,date_updated,beneficiary,type,date,follow_up_types,synthesis,status,help_request,organisation_name,place,redirected,third_person_name,due_date,classified,forwarded_to_justice,interventions,ministre,numero_pegase,signalements,organismes_prescripteurs'
  )
  await insertPermission(
    knex,
    AGENT_D_ACCUEIL_ID,
    'follow_ups',
    'read',
    { _and: [{ beneficiary: BENEFICIARY_AGENT_IN_SAME_ORGANISATION }] },
    'id,user_created,date_created,user_updated,date_updated,beneficiary,type,date,follow_up_types,synthesis,help_request,organisation_name,place,redirected,third_person_name,due_date,classified,forwarded_to_justice,interventions,ministre,numero_pegase,signalements,organismes_prescripteurs,status'
  )
  await insertPermission(
    knex,
    AGENT_D_ACCUEIL_ID,
    'follow_ups',
    'update',
    { _and: [CREATED_BY_CURRENT_USER] },
    'id,user_created,date_created,user_updated,date_updated,beneficiary,type,date,follow_up_types,synthesis,help_request,organisation_name,place,redirected,third_person_name,due_date,classified,forwarded_to_justice,interventions,ministre,numero_pegase,signalements,organismes_prescripteurs'
  )
}

async function createAgentDAccueilFollowUpsFollowUpTypesPermissions(knex) {
  await insertPermission(
    knex,
    AGENT_D_ACCUEIL_ID,
    'follow_ups_follow_up_types',
    'create',
    {},
    '*'
  )
  await insertPermission(
    knex,
    AGENT_D_ACCUEIL_ID,
    'follow_ups_follow_up_types',
    'read',
    {
      _and: [
        {
          follow_ups_id: { beneficiary: BENEFICIARY_AGENT_IN_SAME_ORGANISATION }
        }
      ]
    },
    '*'
  )
  await insertPermission(
    knex,
    AGENT_D_ACCUEIL_ID,
    'follow_ups_follow_up_types',
    'delete',
    { _and: [{ follow_ups_id: CREATED_BY_CURRENT_USER }] }
  )
}

async function createAgentDAccueilHelpRequestsPermissions(knex) {
  await insertPermission(
    knex,
    AGENT_D_ACCUEIL_ID,
    'help_requests',
    'read',
    { _and: [{ beneficiary: BENEFICIARY_AGENT_IN_SAME_ORGANISATION }] },
    'follow_up_types,follow_up_type,beneficiary,id,opening_date,financial_support,external_organisation,status,examination_date,decision_date,handling_date,dispatch_date,user_created,examining_organisation'
  )
}

async function deleteAgentDAccueilRoleAndPermissions(knex) {
  await knex('directus_roles').where({ id: AGENT_D_ACCUEIL_ID }).del()
}
