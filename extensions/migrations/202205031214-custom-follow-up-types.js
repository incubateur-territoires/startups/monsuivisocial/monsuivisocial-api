const { randomUUID } = require('crypto')
const { setSequenceValue } = require('../../utils')
const {
  RESPONSABLE_STRUCTURE_ID,
  TRAVAILLEUR_SOCIAL_ID,
  SAME_ORGANISATION
} = require('../../utils/constants')

const DOMICILIATION = {
  id: randomUUID(),
  name: 'Domiciliation',
  type: 'legal',
  default: true
}
const DEMANDE_D_AIDES_MÉNAGÈRES = {
  id: randomUUID(),
  name: "Demande d'aides ménagères",
  type: 'legal',
  default: true
}
const OBLIGATION_ALIMENTAIRE = {
  id: randomUUID(),
  name: 'Obligation alimentaire',
  type: 'legal',
  default: true
}
const AIDE_MÉDICALE_D_ÉTAT = {
  id: randomUUID(),
  name: "Aide médicale d'État",
  type: 'legal',
  default: true
}
const COMPLÉMENTAIRE_SANTÉ_SOLIDAIRE = {
  id: randomUUID(),
  name: 'Complémentaire santé solidaire',
  type: 'legal',
  default: true
}
const ALLOCATION_DE_SOLIDARITÉ_AUX_PERSONNES_ÂGÉES = {
  id: randomUUID(),
  name: 'Allocation de solidarité aux personnes âgées',
  type: 'legal',
  default: true
}
const ALLOCATION_PERSONNALISÉES_D_AUTONOMIE = {
  id: randomUUID(),
  name: "Allocation personnalisées d'autonomie",
  type: 'legal',
  default: true
}
const REVENU_DE_SOLIDARITÉ_ACTIVE = {
  id: randomUUID(),
  name: 'Revenu de solidarité active',
  type: 'legal',
  default: true
}
const AIDE_SOCIALE = {
  id: randomUUID(),
  name: 'Aide sociale',
  type: 'legal',
  default: true
}
const ENTRÉE_EN_FAMILLE_D_ACCUEIL = {
  id: randomUUID(),
  name: "Entrée en famille d'accueil",
  type: 'legal',
  default: true
}
const ENTRÉE_EN_HÉBERGEMENT_POUR_PERSONNES_ÂGÉES = {
  id: randomUUID(),
  name: 'Entrée en hébergement pour personnes âgées',
  type: 'legal',
  default: true
}
const ENTRÉE_EN_ÉTABLISSEMENT_POUR_PERSONNES_HANDICAPÉES = {
  id: randomUUID(),
  name: 'Entrée en établissement pour personnes handicapées',
  type: 'legal',
  default: true
}
const ACCOMPAGNEMENT_SOCIAL = {
  id: randomUUID(),
  name: 'Accompagnement social',
  type: 'optional',
  default: true
}
const AIDES_FINANCIÈRES_NON_REMBOURSABLES = {
  id: randomUUID(),
  name: 'Aides financières non remboursables',
  type: 'optional',
  default: true
}
const AIDES_FINANCIÈRES_REMBOURSABLES = {
  id: randomUUID(),
  name: 'Aides financières remboursables',
  type: 'optional',
  default: true
}
const ANIMATIONS_SENIORS = {
  id: randomUUID(),
  name: 'Animations seniors',
  type: 'optional',
  default: true
}
const ANIMATIONS_FAMILLES = {
  id: randomUUID(),
  name: 'Animations familles',
  type: 'optional',
  default: true
}
const INCLUSION_NUMÉRIQUE = {
  id: randomUUID(),
  name: 'Inclusion numérique',
  type: 'optional',
  default: true
}
const PLAN_ALERTE_ET_URGENCE = {
  id: randomUUID(),
  name: 'Plan alerte et urgence',
  type: 'optional',
  default: true
}
const AIDE_AU_TRANSPORT = {
  id: randomUUID(),
  name: 'Aide au transport',
  type: 'optional',
  default: true
}
const SOUTIEN_ADMINISTRATIF = {
  id: randomUUID(),
  name: 'Soutien administratif',
  type: 'optional',
  default: true
}
const AIDE_ALIMENTAIRE = {
  id: randomUUID(),
  name: 'Aide alimentaire',
  type: 'optional',
  default: true
}
const PUMA = {
  id: randomUUID(),
  name: 'PUMA',
  type: 'legal',
  default: true
}

const DEFAULT_FOLLOW_UP_TYPES = [
  DOMICILIATION,
  DEMANDE_D_AIDES_MÉNAGÈRES,
  OBLIGATION_ALIMENTAIRE,
  AIDE_MÉDICALE_D_ÉTAT,
  COMPLÉMENTAIRE_SANTÉ_SOLIDAIRE,
  ALLOCATION_DE_SOLIDARITÉ_AUX_PERSONNES_ÂGÉES,
  ALLOCATION_PERSONNALISÉES_D_AUTONOMIE,
  REVENU_DE_SOLIDARITÉ_ACTIVE,
  AIDE_SOCIALE,
  ENTRÉE_EN_FAMILLE_D_ACCUEIL,
  ENTRÉE_EN_HÉBERGEMENT_POUR_PERSONNES_ÂGÉES,
  ENTRÉE_EN_ÉTABLISSEMENT_POUR_PERSONNES_HANDICAPÉES,
  ACCOMPAGNEMENT_SOCIAL,
  AIDES_FINANCIÈRES_NON_REMBOURSABLES,
  AIDES_FINANCIÈRES_REMBOURSABLES,
  ANIMATIONS_SENIORS,
  ANIMATIONS_FAMILLES,
  INCLUSION_NUMÉRIQUE,
  PLAN_ALERTE_ET_URGENCE,
  AIDE_AU_TRANSPORT,
  SOUTIEN_ADMINISTRATIF,
  AIDE_ALIMENTAIRE,
  PUMA
]

module.exports = {
  async up(knex) {
    await createDefaultFollowUpTypes(knex)
    await updatePermissions(knex)
  },

  async down(knex) {
    await knex('follow_up_types').del()

    await knex('organisation_follow_up_types').del()
    await setSequenceValue(
      knex,
      'public.organisation_follow_up_types_id_seq',
      1,
      false
    )

    await knex('directus_permissions')
      .whereIn('collection', [
        'follow_up_types',
        'organisation_follow_up_types'
      ])
      .del()
    await setSequenceValue(knex, 'public.directus_permissions_id_seq', 25)
  }
}

async function createDefaultFollowUpTypes(knex) {
  await knex('follow_up_types').insert(DEFAULT_FOLLOW_UP_TYPES)
}

async function updatePermissions(knex) {
  // It should already be this so it is not undone in down function
  await knex('directus_permissions')
    .where({
      role: RESPONSABLE_STRUCTURE_ID,
      collection: 'organisation',
      action: 'read'
    })
    .update({ fields: '*' })

  await knex('directus_permissions').insert([
    {
      role: RESPONSABLE_STRUCTURE_ID,
      collection: 'follow_up_types',
      action: 'create',
      permissions: {},
      validation: {
        _and: [{ default: { _eq: false } }, { type: { _contains: 'optional' } }]
      },
      presets: { default: false, type: 'optional' },
      fields: '*'
    },
    {
      role: RESPONSABLE_STRUCTURE_ID,
      collection: 'follow_up_types',
      action: 'read',
      permissions: {
        _and: [
          {
            _or: [
              { default: { _eq: true } },
              { organisations: { organisation_id: SAME_ORGANISATION } }
            ]
          }
        ]
      },
      fields: '*'
    },
    {
      role: RESPONSABLE_STRUCTURE_ID,
      collection: 'follow_up_types',
      action: 'update',
      permissions: {
        _and: [
          { organisations: { organisation_id: SAME_ORGANISATION } },
          { default: { _eq: false } }
        ]
      },
      validation: { _and: [{ type: { _contains: 'optional' } }] },
      fields:
        'id,user_created,date_created,user_updated,date_updated,name,type,organisations'
    },
    {
      role: RESPONSABLE_STRUCTURE_ID,
      collection: 'follow_up_types',
      action: 'delete',
      permissions: {
        _and: [{ organisations: { organisation_id: SAME_ORGANISATION } }]
      }
    },
    {
      role: RESPONSABLE_STRUCTURE_ID,
      collection: 'organisation_follow_up_types',
      action: 'create',
      permissions: {},
      fields: '*'
    },
    {
      role: RESPONSABLE_STRUCTURE_ID,
      collection: 'organisation_follow_up_types',
      action: 'read',
      permissions: { _and: [{ organisation_id: SAME_ORGANISATION }] },
      fields: '*'
    },
    {
      role: RESPONSABLE_STRUCTURE_ID,
      collection: 'organisation_follow_up_types',
      action: 'delete',
      permissions: { _and: [{ organisation_id: SAME_ORGANISATION }] }
    },
    {
      role: TRAVAILLEUR_SOCIAL_ID,
      collection: 'follow_up_types',
      action: 'read',
      permissions: {
        _and: [{ organisations: { organisation_id: SAME_ORGANISATION } }]
      },
      fields: '*'
    },
    {
      role: TRAVAILLEUR_SOCIAL_ID,
      collection: 'organisation_follow_up_types',
      action: 'read',
      permissions: { _and: [{ organisation_id: SAME_ORGANISATION }] },
      fields: '*'
    }
  ])

  await setSequenceValue(knex, 'public.directus_permissions_id_seq', 40)
}
