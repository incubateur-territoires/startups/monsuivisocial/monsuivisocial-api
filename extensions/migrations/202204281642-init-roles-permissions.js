const {
  RESPONSABLE_STRUCTURE_ID,
  TRAVAILLEUR_SOCIAL_ID,
  SAME_ORGANISATION,
  DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION,
  USER_IN_SAME_ORGANISATION,
  DEPRECATED_RELATIVE_IN_SAME_ORGANISATION
} = require('../../utils/constants')
const { setSequenceValue } = require('../../utils')

module.exports = {
  async up(knex) {
    await initRoles(knex)

    await setSequenceValue(knex, 'public.directus_permissions_id_seq', 1, false)

    await initResponsableStructurePermissions(knex)
    await initAgentPermissions(knex)

    await setSequenceValue(knex, 'public.directus_permissions_id_seq', 25)
  },

  async down(knex) {
    await knex('directus_roles')
      .whereIn('id', [RESPONSABLE_STRUCTURE_ID, TRAVAILLEUR_SOCIAL_ID])
      .del()

    await knex('directus_permissions')
      .whereIn('role', [RESPONSABLE_STRUCTURE_ID, TRAVAILLEUR_SOCIAL_ID])
      .del()

    await setSequenceValue(knex, 'public.directus_permissions_id_seq', 1, false)
  }
}

async function initRoles(knex) {
  await knex('directus_roles').insert([
    {
      id: RESPONSABLE_STRUCTURE_ID,
      name: 'Responsable de structure',
      description:
        'Responsable de structure. Peut modifier sa structure. Peut créer, modifier et supprimer des bénéficiaires de sa structure. Peut créer et modifier les agents de sa structure.',
      enforce_tfa: false,
      admin_access: false,
      app_access: true
    },
    {
      id: TRAVAILLEUR_SOCIAL_ID,
      name: 'Agent',
      description:
        'Agent de structure. Peut créer et modifier des bénéficiaires de sa structure. Peut consulter sa structure. Peut consulter les utilisateurs de sa structure.',
      enforce_tfa: false,
      admin_access: false,
      app_access: true
    }
  ])
}

async function initResponsableStructurePermissions(knex) {
  await knex('directus_permissions').insert([
    {
      role: RESPONSABLE_STRUCTURE_ID,
      collection: 'beneficiary',
      action: 'create',
      permissions: {},
      fields: '*'
    },
    {
      role: RESPONSABLE_STRUCTURE_ID,
      collection: 'beneficiary',
      action: 'read',
      permissions: {
        _and: [DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION]
      },
      fields: '*'
    },
    {
      role: RESPONSABLE_STRUCTURE_ID,
      collection: 'beneficiary',
      action: 'update',
      permissions: {
        _and: [DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION]
      },
      fields: '*'
    },
    {
      role: RESPONSABLE_STRUCTURE_ID,
      collection: 'beneficiary',
      action: 'delete',
      permissions: {
        _and: [DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION]
      },
      fields: '*'
    },
    {
      role: RESPONSABLE_STRUCTURE_ID,
      collection: 'organisation',
      action: 'read',
      permissions: { _and: [SAME_ORGANISATION] },
      fields:
        'id,date_created,date_updated,user_created,user_updated,type,zip_code,name,city,email,phone,address'
    },
    {
      role: RESPONSABLE_STRUCTURE_ID,
      collection: 'organisation',
      action: 'update',
      permissions: { _and: [SAME_ORGANISATION] },
      fields: '*'
    },
    {
      role: RESPONSABLE_STRUCTURE_ID,
      collection: 'relatives',
      action: 'create',
      permissions: {},
      fields: '*'
    },
    {
      role: RESPONSABLE_STRUCTURE_ID,
      collection: 'relatives',
      action: 'read',
      permissions: { _and: [DEPRECATED_RELATIVE_IN_SAME_ORGANISATION] },
      fields: '*'
    },
    {
      role: RESPONSABLE_STRUCTURE_ID,
      collection: 'relatives',
      action: 'update',
      permissions: { _and: [DEPRECATED_RELATIVE_IN_SAME_ORGANISATION] },
      fields: '*'
    },
    {
      role: RESPONSABLE_STRUCTURE_ID,
      collection: 'directus_users',
      action: 'create',
      permissions: {},
      fields:
        'id,first_name,last_name,email,password,role,language,theme,organisation'
    },
    {
      role: RESPONSABLE_STRUCTURE_ID,
      collection: 'directus_users',
      action: 'read',
      permissions: { _and: [USER_IN_SAME_ORGANISATION] },
      fields: 'organisation'
    },
    {
      role: RESPONSABLE_STRUCTURE_ID,
      collection: 'directus_users',
      action: 'update',
      permissions: { _and: [USER_IN_SAME_ORGANISATION] },
      validation: { _and: [USER_IN_SAME_ORGANISATION] },
      fields: 'first_name,last_name,email,password,organisation,role'
    },
    {
      role: RESPONSABLE_STRUCTURE_ID,
      collection: 'directus_users',
      action: 'delete',
      permissions: { _and: [USER_IN_SAME_ORGANISATION] }
    },
    {
      role: RESPONSABLE_STRUCTURE_ID,
      collection: 'directus_roles',
      action: 'read',
      permissions: {},
      fields: '*'
    }
  ])
}

async function initAgentPermissions(knex) {
  await knex('directus_permissions').insert([
    {
      role: TRAVAILLEUR_SOCIAL_ID,
      collection: 'beneficiary',
      action: 'create',
      permissions: {},
      fields: '*'
    },
    {
      role: TRAVAILLEUR_SOCIAL_ID,
      collection: 'beneficiary',
      action: 'read',
      permissions: {
        _and: [DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION]
      },
      fields: '*'
    },
    {
      role: TRAVAILLEUR_SOCIAL_ID,
      collection: 'beneficiary',
      action: 'update',
      permissions: {
        _and: [DEPRECATED_BENEFICIARY_AGENT_IN_SAME_ORGANISATION]
      },
      fields: '*'
    },
    {
      role: TRAVAILLEUR_SOCIAL_ID,
      collection: 'organisation',
      action: 'read',
      permissions: { _and: [SAME_ORGANISATION] },
      fields: '*'
    },
    {
      role: TRAVAILLEUR_SOCIAL_ID,
      collection: 'relatives',
      action: 'create',
      permissions: {},
      fields: '*'
    },
    {
      role: TRAVAILLEUR_SOCIAL_ID,
      collection: 'relatives',
      action: 'read',
      permissions: { _and: [DEPRECATED_RELATIVE_IN_SAME_ORGANISATION] },
      fields: '*'
    },
    {
      role: TRAVAILLEUR_SOCIAL_ID,
      collection: 'relatives',
      action: 'update',
      permissions: { _and: [DEPRECATED_RELATIVE_IN_SAME_ORGANISATION] },
      fields: '*'
    },
    {
      role: TRAVAILLEUR_SOCIAL_ID,
      collection: 'directus_users',
      action: 'create',
      permissions: {}
    },
    {
      role: TRAVAILLEUR_SOCIAL_ID,
      collection: 'directus_users',
      action: 'read',
      permissions: { _and: [USER_IN_SAME_ORGANISATION] },
      fields: 'organisation'
    },
    {
      role: TRAVAILLEUR_SOCIAL_ID,
      collection: 'directus_users',
      action: 'update',
      permissions: { id: { _eq: '$CURRENT_USER' } },
      fields: 'first_name,last_name,email,password,organisation'
    },
    {
      role: TRAVAILLEUR_SOCIAL_ID,
      collection: 'directus_roles',
      action: 'read',
      permissions: {},
      fields: '*'
    }
  ])
}
