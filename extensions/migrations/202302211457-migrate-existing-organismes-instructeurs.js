module.exports = {
  async up(knex) {
    // Get help requests that has an examining organisation with the organisation of the referents of the beneficiary of the help request
    const helpRequestsWithExaminingOrganisations = await knex('help_requests')
      .select('id', 'examining_organisation', function () {
        this.select(function () {
          this.select('organisation')
            .from('directus_users')
            .whereRaw('id = beneficiary_referents.referent')
        })
          .from('beneficiary_referents')
          .where({ beneficiary: '49039ab9-b786-4bea-bc94-4856a86251b2' })
          .first()
      })
      .whereNotNull('examining_organisation')

    for (const {
      id: helpRequestId,
      examining_organisation: examiningOrganisation,
      organisation
    } of helpRequestsWithExaminingOrganisations) {
      // Insert the organisme instructeur
      const [{ id: organismeInstructeurId }] = await knex(
        'organismes_instructeurs'
      )
        .returning('id')
        .insert({
          name: examiningOrganisation,
          organisation
        })

      // Update the organisme instructeur in the help request
      await knex('help_requests')
        .where({ id: helpRequestId })
        .update({ organisme_instructeur: organismeInstructeurId })
    }
  },

  async down(knex) {
    await knex('organismes_instructeurs').del()
  }
}
