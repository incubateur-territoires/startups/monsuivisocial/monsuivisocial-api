const {
  INSTRUCTEUR_ID,
  BENEFICIARY_AGENT_IN_SAME_ORGANISATION
} = require('../../utils/constants')

module.exports = {
  async up(knex) {
    await knex('directus_permissions')
      .where({
        role: INSTRUCTEUR_ID,
        collection: 'directus_files',
        action: 'read'
      })
      .update({
        permissions: {
          _and: [
            { beneficiary: BENEFICIARY_AGENT_IN_SAME_ORGANISATION },
            { confidential: { _eq: false } }
          ]
        }
      })
    await knex('directus_permissions')
      .where({
        role: INSTRUCTEUR_ID,
        collection: 'directus_files',
        action: 'update'
      })
      .update({
        permissions: {
          _and: [
            { beneficiary: BENEFICIARY_AGENT_IN_SAME_ORGANISATION },
            { confidential: { _eq: false } }
          ]
        }
      })
  },

  async down() {}
}
