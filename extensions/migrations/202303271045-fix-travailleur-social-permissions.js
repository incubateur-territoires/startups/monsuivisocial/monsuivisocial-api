const { TRAVAILLEUR_SOCIAL_ID } = require('../../utils/constants')

module.exports = {
  async up(knex) {
    await knex('directus_permissions')
      .where({
        role: TRAVAILLEUR_SOCIAL_ID,
        collection: 'beneficiary',
        action: 'delete'
      })
      .del()
  },

  async down() {}
}
