#!/bin/sh

set -e
set -v

npx directus schema apply -y snapshot.yaml
npx directus database migrate:latest
