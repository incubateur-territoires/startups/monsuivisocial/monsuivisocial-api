# Mon Suivi Social - Application Server

Mon Suivi Social est une startup d'État portée par [beta.gouv.fr](beta.gouv.fr) et l'[Incubateur des Territoires](https://incubateur.anct.gouv.fr/). Il s'agit d'un outil simple d'utilisation et complet pour gérer l'accompagnement social pour les CCAS.

Cette partie concerne l'API et la base de donnée PostgreSQL de Mon Suivi Social construites avec Directus.

Le code source de l'application Web de Mon Suivi Social est disponible sur un [autre dépôt](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app) dédié.

## Dépendances

Pour lancer le projet, vous avez besoin de :

- Node.js _([lien de téléchargement](https://nodejs.org/en/))_ - Après l'installation, vérifiez que Node.js est correctement installé en exécutant dans le terminal de votre choix la commande `node -v`. Celle-ci doit afficher la version de Node.js installée.
- PostgreSQL _([lien de téléchargement](https://www.postgresql.org/))_.
  - Sur macOS : Il est préférable d'utiliser Homebrew _([instruction d'installation](https://brew.sh/))_
    ```bash
    brew install postgresql
    ```

## Comment contribuer au projet

Avant de commencer, il faut vous assurer d'avoir à la racine du projet [un fichier `.env`](https://docs.directus.io/self-hosted/config-options.html#configuration-files) contenant la configuration minimale requise par Directus. Une base est fournie dans le fichier `.env.dist` qui doit être complétée en fonction de votre configuration personnelle (lignes marquées par une `NOTE:`).

- Installez les dépendances Node.js
  ```bash
  npm install
  ```
- Démarrez PostgreSQL

  ```bash
  docker-compose -f ./docker-compose.dev.yml up -d
  ```

- Installez la base de donnée PostgreSQL locale en paramétrant l'email et le mot de passe de l'utilisateur administrateur

  ```bash
  make install-db
  ```

  Cette commande crée la base de donnée PostgreSQL, la prépare pour Directus, applique le schéma de donnée et les migrations (voir le [Wiki sur les 🦬 migrations](https://gitlab.com/groups/incubateur-territoires/startups/monsuivisocial/-/wikis/directus/%F0%9F%A6%AC%20Migrations)). Ces migrations initialisent notamment le contenu et les configurations Directus de base.

  🚨 Pour le moment [Directus exécute toutes les migrations à l'initialisation](https://github.com/directus/directus/discussions/11798), ce qui n'est pas souhaitable. Le script déplace donc temporairement les migrations pour que celles-ci ne soient pas exécutées avant que le schéma soit prêt.

- **(Optionnel)** Vous pouvez alimenter la base de données avec de la donnée fictive

  ```bash
  make seed-db
  ```

- Démarrez le serveur de développement

  ```bash
  npm start
  ```

  L'application d'administration et l'API sont alors disponibles à l'adresse [localhost:8055](http://localhost:8055/).

  Avant d'accéder à l'application Mon Suivi Social, il est conseillé de lire la section du Wiki [🏁 Pour un démarrage en douceur](https://gitlab.com/groups/incubateur-territoires/startups/monsuivisocial/-/wikis/%F0%9F%8F%81-Pour-un-d%C3%A9marrage-en-douceur).

**À SAVOIR** : Si vous voulez contribuer au projet, merci d'exécuter la commande `npm exec simple-git-hooks` une fois les dépendances Node.js installées pour que votre code soit _linté_ lorsque vous le _commitez_.

## Commandes

### update-db

Applique le _snapshot_ Directus (voir [la documentation](https://docs.directus.io/reference/cli.html#snapshot-the-data-model)) et les dernières migrations (voir [la documentation](https://docs.directus.io/extensions/migrations.html#custom-migrations)). Cette commande est à utiliser dès lors qu'on change de branche.

Usage :

```bash
make update-db
```

⚠️ Il peut être nécessaire de retirer l'application des migrations spécifiques à la branche actuelle avant de basculer sur une autre branche.

### snapshot-db

Met à jour le _snapshot_ Directus (voir [la documentation](https://docs.directus.io/reference/cli.html#snapshot-the-data-model)) et les _dumps_ des configurations de Directus enregistrées en base de donnée. Cette commande est à utiliser lorsque l'on met à jour le modèle de donnée dans Directus ou lorsque l'on rédige une migration en lien avec la configuration de Directus.

Usage :

```bash
make snapshot-db
```

## Documentation

Pour plus d'explications détaillées sur Directus, vous pouvez consulter la [documentation](https://docs.directus.io/) de Directus.

Il existe aussi de le [Wiki du projet](https://gitlab.com/groups/incubateur-territoires/startups/monsuivisocial/-/wikis/home) sur le _versionning_ de la configuration de Directus et son déploiement.

## Environnements

Le Directus de Mon Suivi Social existe sur plusieurs environnements :

- production :
  - Lien d'accès : https://directus.monsuivisocial.incubateur.anct.gouv.fr/
  - Mode de déploiement : Création d'un Git tag
- développement :
  - Lien d'accès : https://directus.monsuivisocial.dev.incubateur.anct.gouv.fr/
  - Mode de déploiement : Poussée sur la branche `main`
- review :
  - Lien d'accès : https://directusreview.monsuivisocial.dev.incubateur.anct.gouv.fr/.
  - Mode de déploiement : Poussée sur n'importe quelle branche `main`
  - NOTE : Côté serveur, il n'existe qu'un seul environnement de review, partagé entre toutes les branches autres que la branche `main`. Par soucis de praticité, il existe aussi une branche `review` qui peut être utilisée pour déployer sur cet environnement. Côté serveur, elle est particulièrement utile pour pouvoir maintenir à jour l'environnement de review et éviter de casser le déploiement suite à une rupture dans le _snapshot_ ou les migrations Directus (voir [cette issue](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/166)).
