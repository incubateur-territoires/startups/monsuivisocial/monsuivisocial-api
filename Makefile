include .env

install-db:
	psql postgres://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/postgres -c "CREATE DATABASE ${DB_DATABASE}"
	mkdir extensions/tmp
	mv extensions/migrations/* extensions/tmp
	npm exec directus bootstrap
	mv extensions/tmp/* extensions/migrations
	rm -r extensions/tmp
	@make update-db

snapshot-db:
	npm exec directus schema snapshot -- --yes ./db/snapshot.yaml
	psql --csv `postgres://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_DATABASE}` -c "SELECT id, name, icon, color, description, status, trigger, accountability, options, operation FROM directus_flows ORDER BY id ASC;" > db/directus-flows.csv
	psql --csv `postgres://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_DATABASE}` -c "SELECT name, parent FROM directus_folders;" > db/directus-folders.csv
	psql --csv `postgres://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_DATABASE}` -c "SELECT id, name, key, type, position_x, position_y, options, resolve, reject, flow FROM directus_operations ORDER BY id ASC;" > db/directus-operations.csv
	psql --csv `postgres://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_DATABASE}` -c "SELECT role, collection, action, permissions, validation, presets, fields FROM directus_permissions ORDER BY role, (collection COLLATE \"C\"), action ASC;" > db/directus-permissions.csv
	psql --csv `postgres://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_DATABASE}` -c "SELECT slug, name, description FROM directus_roles WHERE name != 'Administrator' ORDER BY slug ASC;" > db/directus-roles.csv

update-db:
	npm exec directus schema apply ./db/snapshot.yaml
	npm exec directus database migrate:latest

seed-db:
	node ./db/seed/index.js
