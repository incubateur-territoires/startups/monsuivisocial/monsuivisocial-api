ARG DIRECTUS_VERSION

FROM directus/directus:$DIRECTUS_VERSION
COPY db/snapshot.yaml snapshot.yaml
COPY extensions/migrations extensions/migrations
COPY utils utils

COPY migration.sh migration.sh

ENTRYPOINT ["/directus/migration.sh"]
