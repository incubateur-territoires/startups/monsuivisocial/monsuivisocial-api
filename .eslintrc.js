module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es2021: true
  },
  extends: ['standard', 'prettier'],
  parserOptions: {
    ecmaVersion: 'latest'
  },
  rules: {
    'curly': ['error', 'multi-line'],
    'no-else-return': ['error', { allowElseIf: false }]
  }
}
